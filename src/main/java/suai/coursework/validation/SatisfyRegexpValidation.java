package suai.coursework.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Проверяет переданную строку на соответствие указанным регулярным выражениям. Если
 * набор выражений null или переданное значение null, используется значение nullable.
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SatisfyRegexpValidator.class)
public @interface SatisfyRegexpValidation {
  boolean nullable() default false;
  String[] regexp();

  String message() default "Field check failed";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}
