package suai.coursework.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Constraint(validatedBy = CollectionLengthConstraintValidator.class)
public @interface CollectionLengthConstraint {
  Type type() default Type.EQUAL;
  boolean nullable() default false;
  long value();

  String message() default "Collection length check failed";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

  enum Type {
    EQUAL,
    LESSER,
    GREATER,
    LESSER_OR_EQUAL,
    GREATER_OR_EQUAL,
    NOT_EQUAL
  }
}
