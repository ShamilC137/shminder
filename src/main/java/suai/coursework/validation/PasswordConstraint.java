package suai.coursework.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface PasswordConstraint {
  String[] regexp() default "(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";

  String message() default "must contain 8 symbols, one upper case letter and one digit";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}
