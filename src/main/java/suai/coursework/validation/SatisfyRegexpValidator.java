package suai.coursework.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SatisfyRegexpValidator
    implements ConstraintValidator<SatisfyRegexpValidation, String> {
  private String[] regexp;
  private boolean nullable;

  @Override
  public void initialize(final SatisfyRegexpValidation anno) {
    this.regexp = anno.regexp();
    this.nullable = anno.nullable();
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (regexp == null) {
      return nullable;
    }

    if (value == null) {
      return nullable;
    }

    for (final String exp : regexp) {
      Pattern pattern = Pattern.compile(exp);
      Matcher matcher = pattern.matcher(value);
      if (!matcher.matches()) {
        return false;
      }
    }
    return true;
  }
}
