package suai.coursework.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator  implements ConstraintValidator<PasswordConstraint, String> {
  private String[] regexp;

  @Override
  public void initialize(final PasswordConstraint anno) {
    this.regexp = anno.regexp();
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (regexp == null || value == null) {
      return false;
    }
    for (final String exp : regexp) {
      Pattern pattern = Pattern.compile(exp);
      Matcher matcher = pattern.matcher(value);
      if (!matcher.matches()) {
        return false;
      }
    }
    return true;
  }
}
