package suai.coursework.validation;

import java.util.Collection;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * FIXME: must be replaced with Size (spring-validation)
 */
public class CollectionLengthConstraintValidator implements ConstraintValidator<CollectionLengthConstraint, Collection<?>> {
  private CollectionLengthConstraint anno;

  @Override
  public void initialize(final CollectionLengthConstraint anno) {
    this.anno = anno;
  }

  @Override
  public boolean isValid(Collection value, ConstraintValidatorContext context) {
    if (value == null) {
      return anno.nullable();
    }

    long expected = anno.value();
    long actual = value.size();
    switch (anno.type()) {
      case EQUAL:
        return actual == expected;
      case NOT_EQUAL:
        return actual != expected;
      case LESSER:
        return actual < expected;
      case GREATER:
        return actual > expected;
      case LESSER_OR_EQUAL:
        return actual <= expected;
      case GREATER_OR_EQUAL:
        return actual >= expected;
      default:
        return false;
    }
  }
}
