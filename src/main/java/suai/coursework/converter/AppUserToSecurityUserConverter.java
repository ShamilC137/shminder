package suai.coursework.converter;

import java.util.stream.Collectors;
import org.modelmapper.AbstractConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import suai.coursework.model.AppUser;
import suai.coursework.security.SecurityUser;

@Component
public class AppUserToSecurityUserConverter extends AbstractConverter<AppUser, SecurityUser> {
  @Override
  protected SecurityUser convert(AppUser source) {
    return SecurityUser.builder()
        .id(source.getId())
        .about(source.getAbout())
        .email(source.getEmail())
        .passions(source.getPassions())
        .password(source.getPassword())
        .gender(source.getGender())
        .dateOfBirth(source.getDateOfBirth())
        .authorities(
            source.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList()))
        .firstName(source.getFirstName())
        .lastName(source.getLastName())
        .middleName(source.getMiddleName())
        .locationId(source.getLocationId())
        .createdAt(source.getCreatedAt())
        .enabled(source.isEnabled())
        .phone(source.getPhone())
        .build();
  }
}
