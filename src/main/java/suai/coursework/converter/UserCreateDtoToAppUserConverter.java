package suai.coursework.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;
import suai.coursework.dto.create.UserCreateDto;
import suai.coursework.model.AppUser;
import suai.coursework.service.PassionService;
import suai.coursework.service.RoleService;

@Component
@RequiredArgsConstructor
public class UserCreateDtoToAppUserConverter extends AbstractConverter<UserCreateDto, AppUser> {
  private final PassionService passionService;
  private final RoleService roleService;

  @Override
  protected AppUser convert(UserCreateDto source) {
    return AppUser.builder()
        .dateOfBirth(source.getDateOfBirth())
        .email(source.getEmail())
        .firstName(source.getFirstName())
        .lastName(source.getLastName())
        .middleName(source.getMiddleName())
        .roles(new ArrayList<>(List.of(roleService.getByName("ROLE_USER"))))
        .password(source.getPassword())
        .phone(source.getPhone())
        .locationId(source.getLocationId())
        .passions(
            source.getPassionsIds().stream()
                .map(passionService::getById)
                .collect(Collectors.toList()))
        .gender(source.getGender())
        .about(source.getAbout())
        .build();
  }
}
