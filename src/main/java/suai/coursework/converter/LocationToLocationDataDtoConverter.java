package suai.coursework.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;
import suai.coursework.dto.data.LocationDataDto;
import suai.coursework.model.Location;
import suai.coursework.service.CountryService;

@Component
@RequiredArgsConstructor
public class LocationToLocationDataDtoConverter
    extends AbstractConverter<Location, LocationDataDto> {
  private final CountryService countryService;

  @Override
  protected LocationDataDto convert(Location source) {
    return LocationDataDto.builder()
        .id(source.getId())
        .city(source.getCity())
        .country(countryService.getById(source.getCountryId()))
        .build();
  }
}
