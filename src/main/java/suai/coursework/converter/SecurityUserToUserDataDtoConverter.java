package suai.coursework.converter;

import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.security.SecurityUser;
import suai.coursework.service.LocationService;
import suai.coursework.service.RoleService;

@Component
@RequiredArgsConstructor
public class SecurityUserToUserDataDtoConverter
    extends AbstractConverter<SecurityUser, UserDataDto> {
  private final LocationService locationService;
  private final RoleService roleService;

  @Override
  protected UserDataDto convert(SecurityUser source) {
    return UserDataDto.builder()
        .id(source.getId())
        .firstName(source.getFirstName())
        .lastName(source.getLastName())
        .middleName(source.getMiddleName())
        .dateOfBirth(source.getDateOfBirth())
        .email(source.getEmail())
        .phone(source.getPhone())
        .passions(source.getPassions())
        .roles(
            source.getAuthorities().stream()
                .map(val -> roleService.getByName(val.getAuthority()))
                .collect(Collectors.toList()))
        .location(locationService.getById(source.getLocationId()))
        .about(source.getAbout())
        .gender(source.getGender())
        .build();
  }
}
