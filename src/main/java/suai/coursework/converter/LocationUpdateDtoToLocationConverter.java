package suai.coursework.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;
import suai.coursework.dto.update.LocationUpdateDto;
import suai.coursework.model.Location;
import suai.coursework.service.LocationService;

@Component
@RequiredArgsConstructor
public class LocationUpdateDtoToLocationConverter extends AbstractConverter<LocationUpdateDto, Location> {
  private final LocationService locationService;

  @Override
  protected Location convert(LocationUpdateDto source) {
    final Location old = locationService.getById(source.getId());
    final Location location = new Location();
    location.setId(source.getId());
    if (source.getCity() == null) {
      location.setCity(old.getCity());
    } else {
      location.setCity(source.getCity());
    }

    if (source.getCountryId() == null) {
      location.setCountryId(old.getCountryId());
    } else {
      location.setCountryId(source.getCountryId());
    }
    return location;
  }
}
