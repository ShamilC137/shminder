package suai.coursework.converter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.model.AppUser;
import suai.coursework.model.Passion;
import suai.coursework.model.Role;
import suai.coursework.service.LocationService;
import suai.coursework.service.UserPassionService;
import suai.coursework.service.UserRoleService;

@Component
@RequiredArgsConstructor
public class AppUserToUserDataDtoConverter extends AbstractConverter<AppUser, UserDataDto> {
  private final LocationService locationService;
  private final UserPassionService userPassionService;
  private final UserRoleService userRoleService;

  @Override
  protected UserDataDto convert(AppUser source) {
    final int userId = source.getId();
    List<Role> roles = source.getRoles();
    if (roles == null) {
      roles = userRoleService.findAllByUserId(userId);
    }

    List<Passion> passions = source.getPassions();
    if (passions == null) {
      passions =
          userPassionService.findPassionsByUserId(userId, PageRequest.of(0, Integer.MAX_VALUE));
    }

    return UserDataDto.builder()
        .id(userId)
        .firstName(source.getFirstName())
        .lastName(source.getLastName())
        .middleName(source.getMiddleName())
        .dateOfBirth(source.getDateOfBirth())
        .email(source.getEmail())
        .phone(source.getPhone())
        .passions(passions)
        .roles(roles)
        .location(locationService.getById(source.getLocationId()))
        .about(source.getAbout())
        .gender(source.getGender())
        .build();
  }
}
