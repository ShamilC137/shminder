package suai.coursework.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import suai.coursework.config.properties.UrlSecurityProperties;
import suai.coursework.dto.GlobalErrorDto;
import suai.coursework.exception.security.AccessTokenException;
import suai.coursework.exception.security.ExpiredTokenException;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile("security")
public class AccessTokenFilter extends OncePerRequestFilter {
  private final AccessTokenProvider tokenProvider;
  private final UrlSecurityProperties urlProperties;

  @Override
  public boolean shouldNotFilter(HttpServletRequest request) {
    final var matcher = new AntPathMatcher();
    final var path = request.getRequestURI();
    return matches(matcher, urlProperties.getWhiteList(), path)
        || ("get".equals(request.getMethod().toLowerCase(Locale.ROOT))
            && matches(matcher, urlProperties.getGetWhiteList(), path));
  }

  public boolean matches(final AntPathMatcher matcher, final String[] patterns, final String path) {
    for (final var pattern : patterns) {
      if (matcher.match(pattern, path)) {
        return true;
      }
    }
    return false;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String realToken = null;
    try {
      Optional<String> token = tokenProvider.resolveToken(request);
      if (token.isPresent()) {
        realToken = token.get();
      } else {
        filterChain.doFilter(request, response);
        return;
      }

      tokenProvider.validateToken(realToken);
      Authentication auth = tokenProvider.getAuthentication(realToken);
      SecurityContextHolder.getContext().setAuthentication(auth);
      filterChain.doFilter(request, response);
    } catch (ExpiredTokenException exception) {
      addError(response, HttpStatus.UNAUTHORIZED, "Token had expired");
    } catch (AccessTokenException exception) {
      SecurityContextHolder.clearContext();
      addError(response, exception);
    }
  }

  private void addError(final HttpServletResponse response, final AccessTokenException exception) {
    addError(response, exception.getErrorStatus(), exception.getMessage());
  }

  private void addError(
      final HttpServletResponse response, final HttpStatus errorStatus, final String error) {
    response.setStatus(errorStatus.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    writeError(response, error);
  }

  private void writeError(final HttpServletResponse response, final String error) {
    try {
      new ObjectMapper().writeValue(response.getOutputStream(), new GlobalErrorDto(error));
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }
}
