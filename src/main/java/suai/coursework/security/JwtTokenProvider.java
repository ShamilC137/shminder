package suai.coursework.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import suai.coursework.config.properties.JwtProperties;
import suai.coursework.exception.security.AccessTokenException;
import suai.coursework.exception.security.ExpiredTokenException;

@Component
@Profile("security")
public class JwtTokenProvider implements AccessTokenProvider {
  private final JwtProperties jwtProperties;
  private final String realSecret;
  private final UserDetailsService detailsService;

  @Autowired
  JwtTokenProvider(final JwtProperties properties, UserDetailsService detailsService) {
    this.jwtProperties = properties;
    this.detailsService = detailsService;
    this.realSecret =
        Base64.getEncoder().encodeToString(properties.getSecret().getBytes(StandardCharsets.UTF_8));
  }

  /**
   * Creates token using user information
   *
   * @param user logged user
   * @return access token
   */
  @Override
  public String createToken(final SecurityUser user) {
    Claims claims = Jwts.claims().setSubject(user.getUsername());
    claims.put("firstname", user.getFirstName());
    Date now = new Date();
    Date validity = new Date(now.getTime() + jwtProperties.getExpiration());
    return Jwts.builder()
        .setClaims(claims)
        .setIssuedAt(now)
        .setExpiration(validity)
        .signWith(SignatureAlgorithm.HS256, realSecret)
        .compact();
  }

  /**
   * Validates given access token
   *
   * @param token access token
   * @throws AccessTokenException thrown if access token is invalid
   * @throws ExpiredTokenException thrown if token is expired
   */
  @Override
  public void validateToken(final String token) {
    try {
      Jwts.parser().setSigningKey(realSecret).parseClaimsJws(token);
    } catch (ExpiredJwtException exception) {
      throw new ExpiredTokenException("Token expired", HttpStatus.I_AM_A_TEAPOT); // lol exception
    } catch (JwtException | IllegalArgumentException exception) {
      throw new AccessTokenException("Invalid token", exception, HttpStatus.UNAUTHORIZED);
    }
  }

  /**
   * Return current logged user authentication
   *
   * @param token access token
   * @return user authentication
   * @throws org.springframework.security.core.userdetails.UsernameNotFoundException if user with
   *     extracted name does not exist
   */
  @Override
  public Authentication getAuthentication(final String token) {
    UserDetails details = detailsService.loadUserByUsername(getUsername(token));
    return new UsernamePasswordAuthenticationToken(
        details, details.getPassword(), details.getAuthorities());
  }

  /**
   * Extract username from token
   *
   * @param token access token
   * @return username
   */
  private String getUsername(final String token) {
    return Jwts.parser().setSigningKey(realSecret).parseClaimsJws(token).getBody().getSubject();
  }

  /**
   * Extracts token from request
   *
   * @param request Current http request
   * @return access token
   */
  @Override
  public Optional<String> resolveToken(final HttpServletRequest request) {
    Optional<String> cookie = getTokenFromCookie(request);
    if (cookie.isEmpty()) {
      return Optional.ofNullable(request.getHeader(jwtProperties.getHeader()));
    }
    return cookie;
  }

  private Optional<String> getTokenFromCookie(final HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      return Optional.empty();
    }
    return Arrays.stream(cookies)
        .filter(value -> value.getName().equals(jwtProperties.getCookie()))
        .map(Cookie::getValue)
        .findFirst();
  }
}
