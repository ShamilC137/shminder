package suai.coursework.security.util;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@Profile("mock")
public class MockIssuerValidatorImpl implements IssuerValidator {

  @Override
  public boolean isValid(
      final int userId, final Authentication authentication, final String... additional) {
    return true;
  }
}
