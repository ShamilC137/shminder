package suai.coursework.security.util;

import org.springframework.security.core.Authentication;

public interface IssuerValidator {
  boolean isValid(final int userId, final Authentication authentication, final String... additional);
}
