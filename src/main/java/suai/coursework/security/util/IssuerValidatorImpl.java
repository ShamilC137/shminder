package suai.coursework.security.util;

import java.util.Arrays;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import suai.coursework.security.SecurityUser;
import suai.coursework.util.Authority;

@Component
@Profile("security")
public class IssuerValidatorImpl implements IssuerValidator {
  @Override
  public boolean isValid(int userId, Authentication authentication, final String... additional) {
    SecurityUser user = (SecurityUser) authentication.getPrincipal();
    for (var authority : user.getAuthorities()) {
      final String authorityName = authority.getAuthority();
      if (authorityName.equals(Authority.ADMIN_ROLE) || Arrays.asList(additional).contains(authorityName)) {
        return true;
      }
    }
    return userId == user.getId();
  }
}
