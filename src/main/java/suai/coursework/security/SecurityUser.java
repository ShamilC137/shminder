package suai.coursework.security;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import suai.coursework.model.Passion;
import suai.coursework.util.Gender;

@Validated
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Profile("security")
public class SecurityUser implements UserDetails {
  static final long serialVersionUID = 1L; // UserDetails implements Serializable
  private Integer id;
  private Integer locationId;
  private String firstName;
  private String lastName;
  private String middleName;
  private Gender gender;
  private LocalDate dateOfBirth;
  private String about;
  private String email;
  private String password;
  private boolean enabled;
  private ZonedDateTime createdAt;
  private List<GrantedAuthority> authorities;
  private transient List<Passion> passions;
  private long phone;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }
}
