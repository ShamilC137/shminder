package suai.coursework.security;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;

public interface AccessTokenProvider {
  /**
   * Creates token using user information
   *
   * @param user logged user
   * @return access token
   */
  String createToken(final SecurityUser user);

  /**
   * Validates given access token
   *
   * @param token access token
   * @throws suai.coursework.exception.security.AccessTokenException if token is not present
   */
  void validateToken(final String token);

  /**
   * Return current logged user authentication
   *
   * @param token access token
   * @return user authentication
   */
  Authentication getAuthentication(final String token);

  /**
   * Extracts token from request
   *
   * @param request Current http request
   * @return access token
   */
  Optional<String> resolveToken(final HttpServletRequest request);
}
