package suai.coursework.security;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import suai.coursework.model.AppUser;
import suai.coursework.service.UserService;

@Service
@RequiredArgsConstructor
@Profile("security")
public class JwtUserDetailsService implements UserDetailsService {
  private final UserService userService;
  private final ModelMapper mapper;

  @Override
  public UserDetails loadUserByUsername(@NonNull final String email)
      throws UsernameNotFoundException {
    AppUser user = userService.getByEmail(email);

    return mapper.map(user, SecurityUser.class);
  }
}
