package suai.coursework.controller;

import static suai.coursework.util.Constants.DEFAULT_PAGE;
import static suai.coursework.util.Constants.DEFAULT_SIZE;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import suai.coursework.adapter.CountryAdapter;
import suai.coursework.adapter.LocationAdapter;
import suai.coursework.adapter.PassionAdapter;
import suai.coursework.dto.data.CountryPassionStatisticsDataDto;
import suai.coursework.dto.data.CountryWithStatisticsDataDto;
import suai.coursework.dto.data.LocationWithStatisticsDataDto;
import suai.coursework.dto.data.PassionWithStatisticsDataDto;
import suai.coursework.service.StatisticsService;

@RestController
@RequestMapping("/statistics")
@RequiredArgsConstructor
@Api
public class StatisticsController {
  private final LocationAdapter locationAdapter;
  private final PassionAdapter passionAdapter;
  private final CountryAdapter countryAdapter;
  private final StatisticsService statisticsService;

  @GetMapping("/locations")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает статистику по количеству пользователей для населенных пунктов")
  public List<LocationWithStatisticsDataDto> findLocationsStatistics(
      @PageableDefault final Pageable pageable) {
    return locationAdapter.findAllWithStatistics(pageable);
  }

  @GetMapping("/passions")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает статистику по количеству пользователей для увлечений")
  public List<PassionWithStatisticsDataDto> findPassionsStatistics(
      @PageableDefault final Pageable pageable) {
    return passionAdapter.findAllWithStatistics(pageable);
  }

  @GetMapping("/countries")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает статистику по количеству населенных пунктов для стран")
  public List<CountryWithStatisticsDataDto> findCountriesStatistics(
      @PageableDefault final Pageable pageable) {
    return countryAdapter.findAllWithStatistics(pageable);
  }

  @GetMapping("countries/passions/all")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает статистику для стран по всем увлечениям пользователей данной страны")
  public List<CountryPassionStatisticsDataDto> findAllCountryPassions(
      @RequestParam(defaultValue = DEFAULT_PAGE) final Integer page,
      @RequestParam(defaultValue = DEFAULT_SIZE) final Integer size) {
    return statisticsService.findAllCountriesAndPassionsStatistics(page, size);
  }

  @GetMapping("countries/passions/max")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Возвращает статистику для стран увлечениям с максимальным количеством пользователей данной страны")
  public List<CountryPassionStatisticsDataDto> findMaxCountryPassions(
      @RequestParam(defaultValue = DEFAULT_PAGE) final Integer page,
      @RequestParam(defaultValue = DEFAULT_SIZE) final Integer size) {
    return statisticsService.findCountriesWithMaxPassionUsers(page, size);
  }
}
