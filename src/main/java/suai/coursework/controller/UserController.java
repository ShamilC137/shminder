package suai.coursework.controller;

import static suai.coursework.util.Authority.HAS_ADMIN_ROLE;
import static suai.coursework.util.Authority.HAS_SUPPORT_ROLE;
import static suai.coursework.util.Authority.HAS_USER_ROLE;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;
import suai.coursework.adapter.UserAdapter;
import suai.coursework.dto.create.UserCreateDto;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.exception.InvalidDataException;
import suai.coursework.model.Image;
import suai.coursework.security.SecurityUser;
import suai.coursework.service.ImageService;
import suai.coursework.service.UserService;
import suai.coursework.util.Gender;
import suai.coursework.util.jackson.JsonArg;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Validated
@Api("Контроль для управления пользователями")
public class UserController {
  private final UserAdapter userAdapter;
  private final UserService userService;
  private final ImageService imageService;
  private final ModelMapper mapper;

  @PostMapping("/registration")
  @ApiOperation("Создает нового пользователя")
  @ResponseStatus(HttpStatus.CREATED)
  public UserDataDto registration(@RequestBody @Valid final UserCreateDto dto) {
    return userAdapter.saveUser(dto);
  }

  @GetMapping("/me")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает информацию о текущем пользователе")
  public UserDataDto getMe(@ApiIgnore final Authentication authentication) {
    return mapper.map(authentication.getPrincipal(), UserDataDto.class);
  }

  @PatchMapping("/enabled")
  @PreAuthorize(HAS_ADMIN_ROLE + " || " + HAS_SUPPORT_ROLE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Меняет состояние текущего пользователя (забанен/активен)")
  public void changeUserStatus(
      @ApiParam(required = true) @JsonArg final int userId,
      @ApiParam(required = true) @JsonArg final boolean status,
      @ApiIgnore final Authentication authentication) {
    userAdapter.changeUserStatus(authentication, userId, status);
  }

  @GetMapping("/")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Возвращает список пользователей. Если задан passionId, возвращает пользователей с "
          + "конкретным увлечением, если не задан - всех пользователей с пересекающимися"
          + " с данным пользователем увлечениями")
  public List<UserDataDto> getAllUsersByOptions(
      @RequestParam(required = false) final Integer passionId,
      @ApiParam(required = true) @RequestParam final Gender gender,
      @PageableDefault final Pageable pageable,
      @ApiIgnore final Authentication authentication) {
    if (passionId == null) {
      final int userId = ((SecurityUser) authentication.getPrincipal()).getId();
      return userAdapter.findByPreferences(
          userId, gender, pageable.getPageNumber(), pageable.getPageSize());
    } else {
      return userAdapter.findAllByPassionIdAndGender(passionId, gender, pageable);
    }
  }

  @GetMapping("/likes/from")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize(HAS_USER_ROLE)
  @ApiOperation("Возвращает всех пользователей, которым текущий пользователь поставил лайк")
  public List<UserDataDto> findAllLikesFromMe(
      @ApiIgnore final Authentication authentication, @PageableDefault final Pageable pageable) {
    final int userId = ((SecurityUser) authentication.getPrincipal()).getId();
    return userAdapter.findAllLikedByUser(userId, pageable);
  }

  @GetMapping("/likes/to")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize(HAS_USER_ROLE)
  @ApiOperation("Возвращает всех пользователей, которые поставили лайк текущему пользователю")
  public List<UserDataDto> findAllLikesToMe(
      @ApiIgnore final Authentication authentication, @PageableDefault final Pageable pageable) {
    final int userId = ((SecurityUser) authentication.getPrincipal()).getId();
    return userAdapter.findAllLikesToUser(userId, pageable);
  }

  @PostMapping("/likes")
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize(HAS_USER_ROLE)
  @ApiOperation("Поставить лайк")
  public void like(
      @ApiParam(required = true) @JsonArg final int victimId,
      @ApiIgnore final Authentication authentication) {
    final int initiatorId = ((SecurityUser) authentication.getPrincipal()).getId();
    userAdapter.like(initiatorId, victimId);
  }

  @DeleteMapping("/likes")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(HAS_USER_ROLE)
  @ApiOperation("Удалить лайк")
  public void unlike(
      @ApiParam(required = true) @JsonArg final int victimId,
      @ApiIgnore final Authentication authentication) {
    final int initiatorId = ((SecurityUser) authentication.getPrincipal()).getId();
    userAdapter.unlike(initiatorId, victimId);
  }

  @PostMapping("/passions")
  @PreAuthorize(HAS_USER_ROLE)
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Добавить увлечение текущему пользователю")
  public void addPassionToMe(
      @JsonArg final int passionId, @ApiIgnore final Authentication authentication) {
    userAdapter.addPassionToUser(((SecurityUser) authentication.getPrincipal()).getId(), passionId);
  }

  @DeleteMapping("/passions")
  @PreAuthorize(HAS_USER_ROLE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Удалить увлечение текущего пользователя")
  public void deleteMyPassions(
      @RequestParam final int passionId, @ApiIgnore final Authentication authentication) {
    userAdapter.deletePassionFromUser(
        ((SecurityUser) authentication.getPrincipal()).getId(), passionId);
  }

  @PostMapping(value = "/images")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Добавить фото для профиля для текущего пользователя")
  public void addProfilePhoto(
      @RequestPart final MultipartFile image, @ApiIgnore final Authentication authentication) {
    try {
      final var id = ((SecurityUser) authentication.getPrincipal()).getId();
      imageService.save(new Image(id, image.getBytes()));
    } catch (IOException e) {
      throw new InvalidDataException("Cannot handle image");
    }
  }

  @GetMapping(
      value = "/images",
      produces = {MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE})
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Получить фото пользователя")
  public byte[] getProfilePhoto(@RequestParam final int userId) {
    return imageService.getById(userId).getImageData();
  }
}
