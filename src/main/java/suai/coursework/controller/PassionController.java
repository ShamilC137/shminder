package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import suai.coursework.adapter.PassionAdapter;
import suai.coursework.dto.data.PassionDataDto;
import suai.coursework.dto.groups.UpdateGroup;
import suai.coursework.util.Authority;
import suai.coursework.util.jackson.JsonArg;

@RestController
@RequestMapping("/passions")
@RequiredArgsConstructor
@Api
public class PassionController {
  private final PassionAdapter passionAdapter;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает все увлечения")
  public List<PassionDataDto> findAll(@PageableDefault final Pageable pageable) {
    return passionAdapter.findAll(pageable);
  }

  @GetMapping("{id:^[0-9]+$}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает увлечение по переданному id")
  public PassionDataDto getById(@PathVariable final int id) {
    return passionAdapter.getById(id);
  }

  @GetMapping("{name:^[a-zA-Zа-яА-Я-_]+$}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает увлечение по переданному имени")
  public PassionDataDto getByName(@PathVariable final String name) {
    return passionAdapter.getByName(name);
  }

  @PostMapping
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_MODER_ROLE)
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Создает новое увлечение")
  public PassionDataDto createPassion(@JsonArg final String name) {
    return passionAdapter.save(name);
  }

  @PutMapping
  @PreAuthorize(Authority.HAS_MODER_ROLE + " || " + Authority.HAS_ADMIN_ROLE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Обновляет увлечение (надо ли?)")
  public void update(@RequestBody @Validated(UpdateGroup.class) final PassionDataDto data) {
    passionAdapter.update(data);
  }

  @GetMapping("/users/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает все увлечения пользователя")
  public List<PassionDataDto> findByUserId(
      @PathVariable final int id, @PageableDefault final Pageable pageable) {
    return passionAdapter.findAllByUserId(id, pageable);
  }
}
