package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import suai.coursework.model.Role;
import suai.coursework.service.RoleService;
import suai.coursework.util.Authority;
import suai.coursework.util.jackson.JsonArg;

@RestController
@RequestMapping("/roles")
@RequiredArgsConstructor
@PreAuthorize(Authority.HAS_ADMIN_ROLE)
@Api
public class RoleController {
  private final RoleService roleService;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает список всех колей")
  public List<Role> getAllRoles() {
    return roleService.findAll();
  }

  @GetMapping("/{id:^[0-9]+$}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает роль по переданному id")
  public Role getById(@PathVariable final int id) {
    return roleService.getById(id);
  }

  @GetMapping("/{name:^[a-zA-Zа-яА-Я_-]+$}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает роль по переданному имени")
  public Role getByName(@PathVariable final String name) {
    return roleService.getByName(name);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Создает новую роль")
  public Role createRole(@JsonArg final String name) {
    return roleService.save(Role.builder().name(name).build());
  }

  @PutMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Обновляет роль (надо ли?)")
  public void update(@JsonArg final int id, @JsonArg final String name) {
    final Role role = roleService.getById(id);
    role.setName(name);
    roleService.updateOrSave(role);
  }
}
