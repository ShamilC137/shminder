package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import suai.coursework.adapter.LocationAdapter;
import suai.coursework.dto.create.LocationCreateDto;
import suai.coursework.dto.data.LocationDataDto;
import suai.coursework.dto.update.LocationUpdateDto;
import suai.coursework.util.Authority;

@RestController
@RequestMapping("/locations")
@RequiredArgsConstructor
@Api("Управляет населенными пунктами")
public class LocationController {
  private final LocationAdapter locationAdapter;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Возвращает список населенных пунктов. Если передано countryName, "
          + "возвращает все населенные пункты, принадлежащие данной стране. Если "
          + "передана строка поиска (search), возвращает все населенные пункты, содержащие данную строку."
          + " Если ничего из этого не передано, возвращает все населенные пункты")
  public List<LocationDataDto> findBy(
      @RequestParam(required = false) final String countryName,
      @RequestParam(required = false) final String search,
      @PageableDefault final Pageable pageable) {
    if (countryName != null) {
      if (search != null) {
          return locationAdapter.findAllByCityAndCountry(search, countryName, pageable);
      } else {
        return locationAdapter.findByCountryName(countryName, pageable);
      }
    } else {
      if (search != null) {
        return locationAdapter.findAllByCityStartsWith(search, pageable);
      }
    }

    return locationAdapter.findAll(pageable);
  }

  @GetMapping("{id}")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает населенный пункт по id")
  public LocationDataDto getById(@PathVariable final int id) {
    return locationAdapter.getById(id);
  }

  @GetMapping("name")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает населенный пункт по имени")
  public LocationDataDto getByName(@RequestParam final String city) {
    return locationAdapter.getByCity(city);
  }

  @PostMapping
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_MODER_ROLE)
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Создает новый населенный пункт")
  public LocationDataDto createLocation(@RequestBody @Valid final LocationCreateDto dto) {
    return locationAdapter.save(dto);
  }

  @PutMapping
  @PreAuthorize(Authority.HAS_MODER_ROLE + " || " + Authority.HAS_MODER_ROLE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Изменяет существующий населенный пункт")
  public void update(@RequestBody @Valid final LocationUpdateDto dto) {
    locationAdapter.update(dto);
  }
}
