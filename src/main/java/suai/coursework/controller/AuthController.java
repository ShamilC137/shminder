package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import suai.coursework.adapter.AuthAdapter;
import suai.coursework.dto.data.AuthDataDto;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.service.AuthService;

@RestController
@RequestMapping("/security")
@RequiredArgsConstructor
@Slf4j
@Profile("security")
@Api("Управляет авторизацией")
public class AuthController {
  private final AuthService authService;
  private final AuthAdapter authAdapter;

  @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Авторизация пользователя, ставит токен в куки, также возвращает заголовок с данными (можно явно передавать "
          + "заголовок или будет попытка извлечения из куки")
  public UserDataDto login(
      @RequestBody @Valid final AuthDataDto authDataDto,
      @ApiIgnore final HttpServletResponse response) {
    return authAdapter.login(authDataDto, response);
  }

  @PostMapping("/logout")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Выход из аккаунта, удаляет куки")
  public void logout(
      @ApiIgnore final HttpServletRequest request,
      @ApiIgnore final HttpServletResponse response,
      @ApiIgnore final Authentication authentication) {
    authService.logout(request, response, authentication);
  }
}
