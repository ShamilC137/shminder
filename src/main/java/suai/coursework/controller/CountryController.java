package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import suai.coursework.adapter.CountryAdapter;
import suai.coursework.dto.create.CountryCreateDto;
import suai.coursework.dto.data.CountryDataDto;
import suai.coursework.dto.groups.UpdateGroup;
import suai.coursework.util.Authority;

@RestController
@RequestMapping("/countries")
@RequiredArgsConstructor
@Api("Управляет странами")
public class CountryController {
  private final CountryAdapter countryAdapter;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Если задано name, возвращает все страны, имеющие указанное имя, "
          + "иначе возвращает все страны")
  public List<CountryDataDto> findAll(
      @RequestParam(required = false) final String name, @PageableDefault final Pageable pageable) {
    if (name == null) {
      return countryAdapter.findAll(pageable);
    } else {
      return countryAdapter.findLikeName(name, pageable);
    }
  }

  @GetMapping("/{id}")
  @ApiOperation("Возвращает страну с переданным id")
  @ResponseStatus(HttpStatus.OK)
  public CountryDataDto getById(@PathVariable final int id) {
    return countryAdapter.getById(id);
  }

  @GetMapping("/name")
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает страну с переданным именем")
  public CountryDataDto getByName(@RequestParam final String name) {
    return countryAdapter.getByName(name);
  }

  @PostMapping
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_MODER_ROLE)
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Добавляет новую страну")
  public CountryDataDto createCountry(@RequestBody @Valid final CountryCreateDto dto) {
    return countryAdapter.save(dto);
  }

  @PutMapping
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_MODER_ROLE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ApiOperation("Обновляет данные страны")
  public CountryDataDto update(
      @RequestBody @Validated(UpdateGroup.class) final CountryDataDto data) {
    return countryAdapter.update(data);
  }
}
