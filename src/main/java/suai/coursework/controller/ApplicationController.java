package suai.coursework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import suai.coursework.adapter.ApplicationAdapter;
import suai.coursework.dto.create.ApplicationCreateDto;
import suai.coursework.dto.data.ApplicationDataDto;
import suai.coursework.dto.update.ApplicationUpdateDto;
import suai.coursework.util.ApplicationStatus;
import suai.coursework.util.Authority;

@RestController
@RequestMapping("/applications")
@RequiredArgsConstructor
@Api("Используется для управления заявлениями")
public class ApplicationController {
  private final ApplicationAdapter applicationAdapter;

  @GetMapping
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_SUPPORT_ROLE)
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      "Возвращает список заявлений. Если никаких параметров не передано - возвращает все. "
          + "Если передан userId - возвращает заявления пользователя, если передан status - возвращает "
          + "все заявления с данным статусом, если передано и то, и другое - возвращает заявления с указанным статусом"
          + " для переданного пользователя")
  public List<ApplicationDataDto> findAll(
      @RequestParam(required = false) final Integer userId,
      @RequestParam(required = false) final ApplicationStatus status,
      @PageableDefault final Pageable pageable,
      @ApiIgnore final Authentication authentication) {
    if (status == null) {
      if (userId == null) {
        return applicationAdapter.findAll(pageable);
      } else {
        return applicationAdapter.findByUserId(userId, authentication, pageable);
      }
    } else {
      if (userId == null) {
        return applicationAdapter.findByStatus(status, pageable);
      } else {
        return applicationAdapter.findAllByUserIdAndStatus(
            userId, authentication, status, pageable);
      }
    }
  }

  @GetMapping("{id}")
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_SUPPORT_ROLE)
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation("Возвращает заявление по конкретному id")
  public ApplicationDataDto getById(@ApiIgnore @PathVariable final int id) {
    return applicationAdapter.getById(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Создает заявление")
  public ApplicationDataDto createApplication(@RequestBody @Valid final ApplicationCreateDto dto) {
    return applicationAdapter.save(dto);
  }

  @PatchMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(Authority.HAS_ADMIN_ROLE + " || " + Authority.HAS_SUPPORT_ROLE)
  @ApiOperation("Изменяет статус заявления")
  public void changeStatus(
      @RequestBody @Valid final ApplicationUpdateDto dto,
      @ApiIgnore final Authentication authentication) {
    applicationAdapter.changeApplicationStatus(dto, authentication);
  }
}
