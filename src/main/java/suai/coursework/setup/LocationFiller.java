package suai.coursework.setup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Country;
import suai.coursework.model.Location;
import suai.coursework.service.CountryService;
import suai.coursework.service.LocationService;

@Component
@Slf4j
@Profile("setup")
@RequiredArgsConstructor
public class LocationFiller {
  @JsonIgnoreProperties(ignoreUnknown = true)
  @lombok.Data
  @NoArgsConstructor
  public static class Data {
    @JsonIgnoreProperties(ignoreUnknown = true)
    @lombok.Data
    @NoArgsConstructor
    public static class Country {
      private String country;
      private List<String> cities;
    }

    private List<Country> data;
  }

  private final CountryService countryService;
  private final LocationService locationService;
  private final ObjectMapper mapper;

  @PostConstruct
  public void fill() {
    if (!locationService.findAll(PageRequest.of(0, 1)).isEmpty()) {
      return;
    }

    HttpClient client = HttpClient.newBuilder().build();
    final URI uri = URI.create("https://countriesnow.space/api/v0.1/countries");
    final HttpRequest request = HttpRequest.newBuilder().GET().uri(uri).build();
    Data data;
    try {
      data =
          mapper.readValue(
              client.send(request, HttpResponse.BodyHandlers.ofString()).body(), Data.class);
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new RuntimeException(e);
    }
    for (final var country : data.getData()) {
      try {
        final int actualId = countryService.getByName(country.country).getId();
        for (final var city : country.cities) {
          try {
            locationService.save(Location.builder().countryId(actualId).city(city).build());
          } catch (AlreadyExistException ignored) {
          }
        }
      } catch (NotFoundException ignored) {
      }
    }
  }
}
