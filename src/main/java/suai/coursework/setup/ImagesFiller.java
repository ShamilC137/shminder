package suai.coursework.setup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import suai.coursework.model.AppUser;
import suai.coursework.model.Image;
import suai.coursework.repository.ImageRepo;
import suai.coursework.repository.UserRepo;
import suai.coursework.service.ImageService;

@DependsOn("databaseFiller")
@Profile("setup")
@Component
@RequiredArgsConstructor
public class ImagesFiller {
  private static final String IMAGES_SET_MESSAGE = "images-set";

  private static final String API_HEADER = "Authorization";
  private static final String API_KEY = "563492ad6f917000010000016f1c6c1b361140f88ee463f85e7d274e";
  private static final String BASE_URL =
      "https://api.pexels.com/v1/search?query=natury&per_page=%d&page=%d&size=small";
  private static final Set<String> allowedFormats = Set.of("jpg", "jpeg", "png");
  private static final int MAX_BAD_REQUEST_COUNT = 15;

  private final UserRepo userRepo;
  private final DatabaseFiller databaseFiller;
  private final ImageRepo imageRepo;
  private final ObjectMapper objectMapper;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Answer {
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ImageData {
      @Data
      @NoArgsConstructor
      @AllArgsConstructor
      @JsonIgnoreProperties(ignoreUnknown = true)
      public static class Src {
        private String original;
      }

      private Src src;
    }

    private List<ImageData> photos;
  }

  @PostConstruct
  public void fill() {
    if (databaseFiller.isRecordExists(IMAGES_SET_MESSAGE)) {
      return;
    }

    final var ids =
        userRepo.findAll().stream().map(AppUser::getId).sorted().collect(Collectors.toList());
    final var size = ids.size();
    final var batch = new ArrayList<Image>(size);
    final var images = getImages(size);
    for (int index = 0; index < size; ++index) {
      batch.add(new Image(ids.get(index), images.get(index)));
    }
    imageRepo.saveAll(batch);
    databaseFiller.addRecordToMetaTable(IMAGES_SET_MESSAGE);
  }

  private List<byte[]> getImages(final int requiredAmount) {
    var perPage = Math.min(80, requiredAmount);
    final var result = new ArrayList<byte[]>();
    final var client = HttpClient.newHttpClient();
    for (int page = 1, size = result.size(); size < requiredAmount; ++page, size = result.size()) {
      final var request =createRequest(String.format(BASE_URL, perPage, page));
      try {
        insertAnswer(
            requiredAmount,
            result,
            objectMapper.readValue(
                client.send(request, HttpResponse.BodyHandlers.ofString()).body(), Answer.class),
            client);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }

      perPage = Math.min(80, requiredAmount - size);
    }
    return result;
  }

  private void insertAnswer(final int requiredAmount,
                            final List<byte[]> images,
                            final Answer answer,
                            final HttpClient client) {
    int badRequestCount = 0;
    for (final var photo : answer.getPhotos()) {
      if (images.size() >= requiredAmount) {
        return;
      }

      final var original = photo.src.original;
      final var format = getFileExtension(original);
      if (format == null || !allowedFormats.contains(format)) {
        continue;
      }

      byte[] result;
      try {
          result = client.send(createRequest(original), HttpResponse.BodyHandlers.ofByteArray()).body();
      } catch (Exception e) {
        ++badRequestCount;
        if (badRequestCount >= MAX_BAD_REQUEST_COUNT) {
          throw new RuntimeException(e);
        }
        continue;
      }
      images.add(result);
      badRequestCount = 0;
    }
  }

  private String getFileExtension(final String path) {
    final var index = path.lastIndexOf('.');
    if (index > 0) {
      return path.substring(index + 1);
    } else {
      return null;
    }
  }

  private HttpRequest createRequest(final String uri) {
    return HttpRequest
            .newBuilder()
            .GET()
            .uri(URI.create(uri))
            .header(API_HEADER, API_KEY)
            .build();
  }
}
