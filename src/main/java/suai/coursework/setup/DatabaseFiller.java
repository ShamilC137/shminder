package suai.coursework.setup;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import suai.coursework.config.properties.MetaSqlProperties;
import suai.coursework.util.sql.SqlQueryExecutor;
import suai.coursework.util.sql.exceptions.SQLRuntimeException;

@Component
@Profile("setup")
@Slf4j
@DependsOn("locationFiller")
public class DatabaseFiller {
  private final List<String> sqlFileNames;
  private final String directory;
  private final String metaTableName;
  private final String metaSchemaName;
  private final SqlQueryExecutor sqlQueryExecutor;

  @Autowired
  public DatabaseFiller(
      final MetaSqlProperties metaSqlProperties, final SqlQueryExecutor sqlQueryExecutor) {
    this.sqlQueryExecutor = sqlQueryExecutor;
    this.sqlFileNames = metaSqlProperties.getFiles();
    this.directory = metaSqlProperties.getDirectory();
    this.metaTableName = metaSqlProperties.getMetaTable();
    this.metaSchemaName = metaSqlProperties.getMetaSchema();
  }

  @PostConstruct
  public void fill() {
    createMetaTable();
    for (final var fileName : sqlFileNames) {
      final var path = String.join("/", directory, fileName);
      if (!isRecordExists(path)) {
        sqlQueryExecutor.executeUpdate(getInputStreamReader(path));
        addRecordToMetaTable(path);
      }
    }
  }

  private void createMetaTable() {
    sqlQueryExecutor.executeUpdate(
        String.format(
            "CREATE TABLE IF NOT EXISTS %s.%s (file_name VARCHAR(256) PRIMARY KEY, inserted TIMESTAMP DEFAULT NOW())",
            metaSchemaName, metaTableName));
  }

  public boolean isRecordExists(final String recordName) {
    try {
      final var result =
          sqlQueryExecutor.execute(
              String.format(
                  "SELECT COUNT(*) FROM %s.%s WHERE file_name = '%s'",
                  metaSchemaName, metaTableName, recordName));
      if (!result.next()) {
        throw new IllegalArgumentException();
      }

      if (result.getLong(1) == 0) {
        return false;
      }
    } catch (SQLException e) {
      throw new SQLRuntimeException(e);
    }
    return true;
  }

  public void addRecordToMetaTable(final String recordName) {
    final var query =
        String.format(
            "INSERT INTO %s.%s(file_name) VALUES ('%s')",
            metaSchemaName, metaTableName, recordName);
    if (sqlQueryExecutor.executeUpdate(query) != 1) {
      throw new IllegalArgumentException();
    }
  }

  private InputStreamReader getInputStreamReader(final String path) {
    final var relative = "/" + path + ".sql";
    return new InputStreamReader(
        Objects.requireNonNull(this.getClass().getResourceAsStream(relative)));
  }
}
