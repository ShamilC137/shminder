package suai.coursework.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import suai.coursework.model.AppUser;
import suai.coursework.model.Like;

public interface LikeRepo extends JpaRepository<Like, Like.LikeKey> {
  @Query(
          "SELECT u FROM users u INNER JOIN likes l ON u.id = l.key.victim WHERE l.key.initiator = :initiatorId")
  List<AppUser> findAppUserByInitiatorId(final int initiatorId, final Pageable pageable);

  @Query("SELECT u FROM users u INNER JOIN likes l ON u.id = l.key.initiator WHERE l.key.victim = :victimId")
  List<AppUser> findAppUserByVictimId(final int victimId, final Pageable pageable);
}
