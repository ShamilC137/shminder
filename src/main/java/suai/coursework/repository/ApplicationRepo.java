package suai.coursework.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import suai.coursework.model.Application;
import suai.coursework.util.ApplicationStatus;

public interface ApplicationRepo extends JpaRepository<Application, Integer> {
  @Modifying
  @Query("UPDATE applications SET status = :newStatus WHERE id = :applicationId")
  int changeApplicationStatus(final int applicationId, final ApplicationStatus newStatus);

  List<Application> findByStatus(final ApplicationStatus status, final Pageable pageable);

  List<Application> findByUserId(final int userId, final Pageable pageable);

  List<Application> findByUserIdAndStatus(final int userId, final ApplicationStatus status, final Pageable pageable);
}
