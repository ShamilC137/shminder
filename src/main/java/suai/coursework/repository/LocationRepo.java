package suai.coursework.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import suai.coursework.model.Location;

public interface LocationRepo extends JpaRepository<Location, Integer> {
  Optional<Location> findByCityIgnoreCase(final String city);

  List<Location> findAllByCountryId(final int countryId, final Pageable pageable);

  List<Location> findAllByCityStartsWithIgnoreCase(final String name, final Pageable pageable);

  List<Location> findAllByCountryIdAndCityStartingWithIgnoreCase(
      final int countryId, final String cityPrefix, final Pageable pageable);
}
