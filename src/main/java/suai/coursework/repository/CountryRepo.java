package suai.coursework.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import suai.coursework.model.Country;

public interface CountryRepo extends JpaRepository<Country, Integer> {
  Optional<Country> findByNameIgnoreCase(final String name);

  List<Country> findByNameStartsWithIgnoreCase(final String name, final Pageable pageable);
}
