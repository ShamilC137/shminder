package suai.coursework.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import suai.coursework.model.AppUser;
import suai.coursework.model.Passion;
import suai.coursework.model.UserPassion;
import suai.coursework.util.Gender;

public interface UserPassionRepo extends JpaRepository<UserPassion, UserPassion.UserPassionKey> {
  @Query(
      "SELECT p FROM passions p INNER JOIN users_passions up ON p.id = up.key.passionId WHERE up.key.userId = :userId")
  List<Passion> findPassionsByUserId(final int userId, final Pageable pageable);

  @Query(
      "SELECT u FROM users u INNER JOIN users_passions up ON u.id = up.key.userId WHERE up.key.passionId = :passionId AND u.gender = :gender")
  List<AppUser> findUsersByPassionIdAndGender(final int passionId, final Gender gender, final Pageable pageable);
}
