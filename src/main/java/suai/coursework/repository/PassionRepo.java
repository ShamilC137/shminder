package suai.coursework.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import suai.coursework.model.Passion;

@Repository
public interface PassionRepo extends JpaRepository<Passion, Integer> {
  Optional<Passion> findByNameIgnoreCase(final String name);
}
