package suai.coursework.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import suai.coursework.model.AppUser;

@Repository
public interface UserRepo extends JpaRepository<AppUser, Integer> {
  Optional<AppUser> findAppUserByEmailIgnoreCase(final String email);

  @Modifying
  @Query(value = "UPDATE users SET enabled = :enabled WHERE id = :id")
  int setEnableStatusById(final int id, final boolean enabled);
}
