package suai.coursework.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import suai.coursework.model.Role;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
  Optional<Role> findByNameIgnoreCase(final String name);
}
