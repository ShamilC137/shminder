package suai.coursework.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import suai.coursework.model.Role;
import suai.coursework.model.UserRole;

public interface UserRoleRepo extends JpaRepository<UserRole, UserRole.UserRoleKey> {
  @Query("SELECT r FROM users_roles ur INNER JOIN roles r ON ur.key.roleId = r.id WHERE ur.key.userId = :userId")
  List<Role> findAllByUserId(final int userId);
}
