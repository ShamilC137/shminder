package suai.coursework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import suai.coursework.model.Image;

public interface ImageRepo extends JpaRepository<Image, Integer> {
}
