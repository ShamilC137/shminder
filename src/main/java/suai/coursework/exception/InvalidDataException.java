package suai.coursework.exception;

import org.springframework.http.HttpStatus;

public class InvalidDataException extends GlobalException {
  public InvalidDataException() {
  }

  public InvalidDataException(final String message) {
    super(message);
  }

  public InvalidDataException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public InvalidDataException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public InvalidDataException(
          final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
