package suai.coursework.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GlobalException {
  public static final String BASIC_MESSAGE = "%s with given %s not found";

  public NotFoundException(final String message) {
    super(message);
  }

  public NotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NotFoundException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public NotFoundException(
      final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
