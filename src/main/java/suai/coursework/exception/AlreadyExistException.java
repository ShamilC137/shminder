package suai.coursework.exception;

import org.springframework.http.HttpStatus;

public class AlreadyExistException extends GlobalException {
  public static final String BASIC_MESSAGE = "%s with given %s has already existed!";

  public AlreadyExistException(final String message) {
    super(message);
  }

  public AlreadyExistException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public AlreadyExistException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public AlreadyExistException(
      final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
