package suai.coursework.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class GlobalException extends RuntimeException {
  private final HttpStatus errorStatus;

  public GlobalException() {
    this.errorStatus = HttpStatus.BAD_REQUEST;
  }

  public GlobalException(final String message) {
    super(message);
    this.errorStatus = HttpStatus.BAD_REQUEST;
  }

  public GlobalException(final String message, final Throwable cause) {
    super(message, cause);
    this.errorStatus = HttpStatus.BAD_REQUEST;
  }

  public GlobalException(final String message, final HttpStatus errorStatus) {
    super(message);
    this.errorStatus = errorStatus;
  }

  public GlobalException(
      final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause);
    this.errorStatus = errorStatus;
  }
}
