package suai.coursework.exception.security;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import suai.coursework.exception.GlobalException;

@NoArgsConstructor
public class AccessTokenException extends GlobalException {
  public AccessTokenException(final String message) {
    super(message);
  }

  public AccessTokenException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public AccessTokenException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public AccessTokenException(
      final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
