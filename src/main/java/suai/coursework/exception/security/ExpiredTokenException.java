package suai.coursework.exception.security;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
public class ExpiredTokenException extends AccessTokenException {
  public ExpiredTokenException(final String message) {
    super(message);
  }

  public ExpiredTokenException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ExpiredTokenException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public ExpiredTokenException(
          final HttpStatus errorStatus, final String message, final Throwable cause) {
    super(message, cause, errorStatus);
  }
}
