package suai.coursework.exception;

import org.springframework.http.HttpStatus;

public class DisabledUserException extends GlobalException {
  public DisabledUserException() {
    super("User disabled!");
  }

  public DisabledUserException(final String message) {
    super(message);
  }

  public DisabledUserException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public DisabledUserException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public DisabledUserException(
          final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
