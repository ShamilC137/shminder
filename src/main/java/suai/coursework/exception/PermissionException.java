package suai.coursework.exception;

import org.springframework.http.HttpStatus;

public class PermissionException extends GlobalException {
  public PermissionException() {
    super("Have no permission", HttpStatus.FORBIDDEN);
  }

  public PermissionException(final String message) {
    super(message, HttpStatus.FORBIDDEN);
  }

  public PermissionException(final String message, final Throwable cause) {
    super(message, cause, HttpStatus.FORBIDDEN);
  }

  public PermissionException(final String message, final HttpStatus errorStatus) {
    super(message, errorStatus);
  }

  public PermissionException(
          final String message, final Throwable cause, final HttpStatus errorStatus) {
    super(message, cause, errorStatus);
  }
}
