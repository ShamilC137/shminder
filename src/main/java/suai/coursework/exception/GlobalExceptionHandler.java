package suai.coursework.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import suai.coursework.dto.GlobalErrorDto;
import suai.coursework.exception.security.AccessTokenException;

@RestControllerAdvice
@ControllerAdvice
public class GlobalExceptionHandler {
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<GlobalErrorDto> handleMethodArgumentsValidationExceptions(
      MethodArgumentNotValidException ex) {
    var error = ex.getBindingResult().getAllErrors().stream().findAny();
    if (error.isEmpty()) {
      return ResponseEntity.badRequest().body(new GlobalErrorDto("Cannot define problem"));
    }
    FieldError fieldError = ((FieldError) error.get());
    return ResponseEntity.badRequest()
        .body(new GlobalErrorDto(fieldError.getField() + ": " + fieldError.getDefaultMessage()));
  }

  @ExceptionHandler(GlobalException.class)
  public ResponseEntity<GlobalErrorDto> handleGlobalError(final GlobalException exception) {
    return ResponseEntity.status(exception.getErrorStatus())
        .body(new GlobalErrorDto(exception.getMessage()));
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<GlobalErrorDto> handleConstraintViolationException(
      ConstraintViolationException exception) {
    if (exception.getConstraintViolations().isEmpty()) {
      return ResponseEntity.badRequest().body(new GlobalErrorDto("Server error"));
    } else {
      ConstraintViolation<?> violation = exception.getConstraintViolations().iterator().next();
      String fieldName = violation.getPropertyPath().toString().split("[.]")[1];
      return ResponseEntity.badRequest()
          .body(new GlobalErrorDto(fieldName + ": " + violation.getMessage()));
    }
  }

  @ExceptionHandler(BindException.class)
  public ResponseEntity<List<GlobalErrorDto>> handleBindException(BindException exception) {
    final List<GlobalErrorDto> errors = new ArrayList<>();
    for (FieldError error : exception.getFieldErrors()) {
      StringBuilder message = new StringBuilder();
      message.append(error.getField());
      message.append(": ");
      String errorMessage = error.getDefaultMessage();
      message.append(
          Objects.requireNonNullElse(errorMessage, "constraint failed, please, check value"));
      errors.add(new GlobalErrorDto(message.toString()));
    }
    return ResponseEntity.badRequest().body(errors);
  }

  @ExceptionHandler(AccessTokenException.class)
  public ResponseEntity<GlobalErrorDto> handleAccessTokenException(AccessTokenException exception) {
    return handleException(exception, exception.getErrorStatus());
  }

  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity<GlobalErrorDto> handleAuthenticationException(
      AuthenticationException exception) {
    return handleException(exception, HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(DataAccessException.class)
  public ResponseEntity<GlobalErrorDto> handleDataAccessException(
      final DataAccessException exception) {
    return handleException(exception, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private ResponseEntity<GlobalErrorDto> handleException(
      Exception exception, HttpStatus errorStatus) {
    Throwable cause = exception.getCause();
    return ResponseEntity.status(errorStatus)
        .contentType(MediaType.APPLICATION_JSON)
        .body(new GlobalErrorDto(exception.getMessage()));
  }
}
