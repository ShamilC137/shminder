package suai.coursework.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "users_roles")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRole {
  @Embeddable
  @Data
  @Builder
  @NoArgsConstructor
  @AllArgsConstructor
  public static class UserRoleKey implements Serializable {
    @Column(name = "fk_user_id")
    private int userId;

    @Column(name = "fk_role_id")
    private int roleId;
  }

  @EmbeddedId private UserRoleKey key;
}
