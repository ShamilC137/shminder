package suai.coursework.model;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import suai.coursework.util.ApplicationStatus;

@Entity(name = "applications")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Application {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private ZonedDateTime createdAt;

  @Enumerated(EnumType.ORDINAL)
  private ApplicationStatus status;

  @Column(name = "fk_desired_role_id")
  private Integer desiredRoleId;

  @Column(name = "fk_user_id")
  private Integer userId;
}
