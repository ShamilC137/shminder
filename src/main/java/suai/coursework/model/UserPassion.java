package suai.coursework.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "users_passions")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPassion {
  @Embeddable
  @Setter
  @Getter
  @Builder
  @NoArgsConstructor
  @AllArgsConstructor
  public static class UserPassionKey implements Serializable {
    @Column(name = "fk_user_id")
    private int userId;

    @Column(name = "fk_passion_id")
    private int passionId;
  }

  public UserPassion(final int userId, final int passionId) {
    key = new UserPassionKey(userId, passionId);
  }

  @EmbeddedId private UserPassionKey key;
}
