package suai.coursework.model;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import suai.coursework.util.Gender;

@Entity(name = "users")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class AppUser {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "fk_location_id")
  private Integer locationId;

  private String firstName;
  private String lastName;
  private String middleName;

  @Enumerated(value = EnumType.STRING)
  private Gender gender;

  private LocalDate dateOfBirth;
  private String about;

  private String email;
  private String password;

  private boolean enabled;
  private ZonedDateTime createdAt;

  @Transient
  private List<Role> roles;

  @Transient
  private List<Passion> passions;

  private long phone;
}
