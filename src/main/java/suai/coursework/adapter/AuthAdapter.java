package suai.coursework.adapter;

import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import suai.coursework.dto.data.AuthDataDto;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.service.AuthService;

@Component
@RequiredArgsConstructor
public class AuthAdapter {
  private final ModelMapper modelMapper;
  private final AuthService authService;

  public UserDataDto login(final AuthDataDto authDataDto, final HttpServletResponse response) {
    return modelMapper.map(
        authService.login(authDataDto.getUsername(), authDataDto.getPassword(), response),
        UserDataDto.class);
  }
}
