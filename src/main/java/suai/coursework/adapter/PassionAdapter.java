package suai.coursework.adapter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import suai.coursework.dto.data.PassionDataDto;
import suai.coursework.dto.data.PassionWithStatisticsDataDto;
import suai.coursework.exception.InvalidDataException;
import suai.coursework.model.Passion;
import suai.coursework.service.PassionService;
import suai.coursework.service.UserPassionService;
import suai.coursework.util.MapperUtility;

@Component
@RequiredArgsConstructor
public class PassionAdapter {
  private final PassionService passionService;
  private final UserPassionService userPassionService;
  private final ModelMapper mapper;
  private final MapperUtility mapperUtility;

  public List<PassionDataDto> findAll(final Pageable pageable) {
    return mapperUtility.convertToDtoList(passionService.findAll(pageable), PassionDataDto.class);
  }

  public PassionDataDto getById(final int id) {
    return mapper.map(passionService.getById(id), PassionDataDto.class);
  }

  public PassionDataDto getByName(final String name) {
    return mapper.map(passionService.getByName(name), PassionDataDto.class);
  }

  public PassionDataDto save(final String name) {
    return mapper.map(
        passionService.save(Passion.builder().name(name).build()), PassionDataDto.class);
  }

  public PassionDataDto update(final PassionDataDto dto) {
    passionService.getById(dto.getId());
    final Passion passion = mapper.map(dto, Passion.class);
    if (passion.getName() == null) {
      throw new InvalidDataException("Must contain name!");
    }
    return mapper.map(passionService.updateOrSave(passion), PassionDataDto.class);
  }

  public List<PassionDataDto> findAllByUserId(final int userId, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        userPassionService.findPassionsByUserId(userId, pageable), PassionDataDto.class);
  }

  public List<PassionWithStatisticsDataDto> findAllWithStatistics(final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        passionService.findAll(pageable), PassionWithStatisticsDataDto.class);
  }
}
