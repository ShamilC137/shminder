package suai.coursework.adapter;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import suai.coursework.dto.create.UserCreateDto;
import suai.coursework.dto.data.UserDataDto;
import suai.coursework.exception.NotFoundException;
import suai.coursework.exception.PermissionException;
import suai.coursework.model.AppUser;
import suai.coursework.model.Like;
import suai.coursework.model.UserPassion;
import suai.coursework.security.util.IssuerValidator;
import suai.coursework.service.LikeService;
import suai.coursework.service.RoleService;
import suai.coursework.service.UserPassionService;
import suai.coursework.service.UserService;
import suai.coursework.util.Authority;
import suai.coursework.util.Gender;
import suai.coursework.util.MapperUtility;

@Component
@RequiredArgsConstructor
public class UserAdapter {
  private final UserService userService;
  private final LikeService likeService;
  private final RoleService roleService;
  private final UserPassionService userPassionService;
  private final IssuerValidator issuerValidator;
  private final ModelMapper mapper;
  private final MapperUtility mapperUtility;

  public UserDataDto saveUser(final UserCreateDto dto) {
    final var data = mapper.map(dto, AppUser.class);
    data.setCreatedAt(ZonedDateTime.now());
    data.setEnabled(true);
    data.setRoles(new ArrayList<>(List.of(roleService.getByName(Authority.USER_ROLE))));
    return mapper.map(userService.save(data), UserDataDto.class);
  }

  public List<UserDataDto> findAllByPassionIdAndGender(
          final int passionId, final Gender gender, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        userPassionService.findUsersByPassionIdAndGender(passionId, gender, pageable),
        UserDataDto.class);
  }

  public List<UserDataDto> findAllLikedByUser(final int userId, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        likeService.findAllByUserLikes(userId, pageable), UserDataDto.class);
  }

  public List<UserDataDto> findAllLikesToUser(final int userId, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        likeService.findAllToUserLikes(userId, pageable), UserDataDto.class);
  }

  public void like(final int initiatorId, final int victimId) {
    likeService.save(
        new Like(Like.LikeKey.builder().initiator(initiatorId).victim(victimId).build()));
  }

  public void unlike(final int initiatorId, final int victimId) {
    likeService.remove(Like.LikeKey.builder().initiator(initiatorId).victim(victimId).build());
  }

  public List<UserDataDto> findByPreferences(
      final int userId, final Gender gender, final int page, final int size) {
    return mapperUtility.convertToDtoList(
        userPassionService.findByPreferences(userId, gender.name(), page, size), UserDataDto.class);
  }

  public void changeUserStatus(
      final Authentication authentication, final int userId, final boolean status) {
    if (!issuerValidator.isValid(userId, authentication, Authority.SUPPORT_ROLE)) {
      throw new PermissionException();
    }

    if (userService.setUserEnabledStatus(userId, status) == 0) {
      throw new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "User", "id"));
    }
  }

  public void addPassionToUser(final int userId, final int passionId) {
    userPassionService.save(new UserPassion(userId, passionId));
  }

  public void deletePassionFromUser(final int userId, final int passionId) {
    userPassionService.delete(new UserPassion(userId, passionId));
  }
}
