package suai.coursework.adapter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import suai.coursework.dto.create.CountryCreateDto;
import suai.coursework.dto.data.CountryDataDto;
import suai.coursework.dto.data.CountryWithStatisticsDataDto;
import suai.coursework.model.Country;
import suai.coursework.service.CountryService;
import suai.coursework.util.MapperUtility;

@Component
@RequiredArgsConstructor
public class CountryAdapter {
  private final CountryService countryService;
  private final ModelMapper mapper;
  private final MapperUtility mapperUtility;

  public CountryDataDto getById(final int id) {
    return mapper.map(countryService.getById(id), CountryDataDto.class);
  }

  public CountryDataDto getByName(final String name) {
    return mapper.map(countryService.getByName(name), CountryDataDto.class);
  }

  public List<CountryDataDto> findLikeName(final String name, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        countryService.findLikeName(name, pageable), CountryDataDto.class);
  }

  public CountryDataDto save(final CountryCreateDto data) {
    final Country country = mapper.map(data, Country.class);
    return mapper.map(countryService.save(country), CountryDataDto.class);
  }

  public CountryDataDto update(final CountryDataDto data) {
    final Country old = countryService.getById(data.getId());
    final Country country = mapper.map(data, Country.class);
    if (country.getCountryCode() == null) {
      country.setCountryCode(old.getCountryCode());
    }
    if (country.getName() == null) {
      country.setName(old.getName());
    }
    return mapper.map(countryService.updateOrSave(country), CountryDataDto.class);
  }

  public List<CountryDataDto> findAll(final Pageable pageable) {
    return mapperUtility.convertToDtoList(countryService.findAll(pageable), CountryDataDto.class);
  }

  public List<CountryWithStatisticsDataDto> findAllWithStatistics(final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        countryService.findAll(pageable), CountryWithStatisticsDataDto.class);
  }
}
