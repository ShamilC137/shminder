package suai.coursework.adapter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import suai.coursework.dto.create.LocationCreateDto;
import suai.coursework.dto.data.LocationDataDto;
import suai.coursework.dto.data.LocationWithStatisticsDataDto;
import suai.coursework.dto.update.LocationUpdateDto;
import suai.coursework.model.Location;
import suai.coursework.service.LocationService;
import suai.coursework.util.MapperUtility;

@Component
@RequiredArgsConstructor
public class LocationAdapter {
  private final LocationService locationService;
  private final ModelMapper mapper;
  private final MapperUtility mapperUtility;

  public List<LocationDataDto> findAll(final Pageable pageable) {
    return mapperUtility.convertToDtoList(locationService.findAll(pageable), LocationDataDto.class);
  }

  public List<LocationWithStatisticsDataDto> findAllWithStatistics(final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        locationService.findAll(pageable), LocationWithStatisticsDataDto.class);
  }

  public List<LocationDataDto> findByCountryName(final String name, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        locationService.findAllByCountryName(name, pageable), LocationDataDto.class);
  }

  public LocationDataDto getById(final int id) {
    return mapper.map(locationService.getById(id), LocationDataDto.class);
  }

  public LocationDataDto getByCity(final String city) {
    return mapper.map(locationService.getByCity(city), LocationDataDto.class);
  }

  public LocationDataDto save(final LocationCreateDto data) {
    final Location location = mapper.map(data, Location.class);
    return mapper.map(locationService.save(location), LocationDataDto.class);
  }

  public LocationDataDto update(final LocationUpdateDto data) {
    final Location location = mapper.map(data, Location.class);
    return mapper.map(locationService.updateOrSave(location), LocationDataDto.class);
  }

  public List<LocationDataDto> findAllByCityStartsWith(final String city, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        locationService.findAllByCityStartsWith(city, pageable), LocationDataDto.class);
  }

  public List<LocationDataDto> findAllByCityAndCountry(
      final String cityPrefix, final String countryName, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        locationService.findAllByCityAndCountry(countryName, cityPrefix, pageable),
        LocationDataDto.class);
  }
}
