package suai.coursework.adapter;

import static suai.coursework.util.Authority.SUPPORT_ROLE;

import java.time.ZonedDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import suai.coursework.dto.create.ApplicationCreateDto;
import suai.coursework.dto.data.ApplicationDataDto;
import suai.coursework.dto.update.ApplicationUpdateDto;
import suai.coursework.exception.PermissionException;
import suai.coursework.model.Application;
import suai.coursework.security.util.IssuerValidator;
import suai.coursework.service.ApplicationService;
import suai.coursework.util.ApplicationStatus;
import suai.coursework.util.MapperUtility;

@Component
@RequiredArgsConstructor
public class ApplicationAdapter {
  private final ApplicationService applicationService;
  private final ModelMapper mapper;
  private final MapperUtility mapperUtility;
  private final IssuerValidator issuerValidator;

  public ApplicationDataDto getById(final int id) {
    return mapper.map(applicationService.getById(id), ApplicationDataDto.class);
  }

  public List<ApplicationDataDto> findByUserId(
      final int userId, final Authentication authentication, final Pageable pageable) {
    if (!issuerValidator.isValid(userId, authentication, SUPPORT_ROLE)) {
      throw new PermissionException();
    }
    return mapperUtility.convertToDtoList(
        applicationService.findByUserId(userId, pageable), ApplicationDataDto.class);
  }

  public List<ApplicationDataDto> findByStatus(
      final ApplicationStatus status, final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        applicationService.findByStatus(status, pageable), ApplicationDataDto.class);
  }

  public ApplicationDataDto save(final ApplicationCreateDto data) {
    final Application application = mapper.map(data, Application.class);
    application.setCreatedAt(ZonedDateTime.now());
    application.setStatus(ApplicationStatus.NEW);
    return mapper.map(applicationService.save(application), ApplicationDataDto.class);
  }

  public int changeApplicationStatus(
      final ApplicationUpdateDto dto, final Authentication authentication) {
    final int applicationId = dto.getApplicationId();
    final Application application = applicationService.getById(applicationId);
    if (!issuerValidator.isValid(application.getUserId(), authentication, SUPPORT_ROLE)) {
      throw new PermissionException();
    }
    return applicationService.changeApplicationStatus(applicationId, dto.getStatus());
  }

  public List<ApplicationDataDto> findAll(final Pageable pageable) {
    return mapperUtility.convertToDtoList(
        applicationService.findAll(pageable), ApplicationDataDto.class);
  }

  public List<ApplicationDataDto> findAllByUserIdAndStatus(
      final int userId,
      final Authentication authentication,
      final ApplicationStatus status,
      final Pageable pageable) {
    if (!issuerValidator.isValid(userId, authentication, SUPPORT_ROLE)) {
      throw new PermissionException("You have no rights to see applications");
    }
    return mapperUtility.convertToDtoList(
        applicationService.findByUserIdAndStatus(userId, status, pageable),
        ApplicationDataDto.class);
  }
}
