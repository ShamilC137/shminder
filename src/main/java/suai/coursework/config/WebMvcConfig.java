package suai.coursework.config;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import suai.coursework.config.properties.CorsProperties;
import suai.coursework.util.jackson.JsonPathArgumentResolver;

@Configuration
@Profile("security")
@RequiredArgsConstructor
public class WebMvcConfig implements WebMvcConfigurer {
  private final CorsProperties corsProperties;

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    argumentResolvers.add(new JsonPathArgumentResolver());
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry
            .addMapping("/**")
            .allowedHeaders(corsProperties.getAllowedHeaders().toArray(new String[]{}))
            .allowedMethods(corsProperties.getAllowedMethods().toArray(new String[]{}))
            .allowedOrigins(corsProperties.getAllowedOrigins().toArray(new String[]{}))
            .exposedHeaders(corsProperties.getExposedHeaders().toArray(new String[]{}));
  }
}
