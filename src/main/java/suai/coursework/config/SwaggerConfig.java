package suai.coursework.config;

import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.ApiParam;
import java.util.Collections;
import java.util.List;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.data.domain.Pageable;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRuleConvention;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import suai.coursework.config.properties.JwtProperties;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
  private final JwtProperties properties;
  private static final String API_KEY_NAME = "token";

  public SwaggerConfig(@Autowired(required = false) JwtProperties properties) {
    this.properties = properties;
  }

  @Bean
  public Docket apiV2() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(
            RequestHandlerSelectors.basePackage("suai.coursework.controller"))
        .paths(PathSelectors.any())
        .build()
        .securityContexts(securityContexts())
        .securitySchemes(Collections.singletonList(apiKey()));
  }

  private ApiKey apiKey() {
    if (properties == null) {
      return new ApiKey("UNUSED", "UNUSED", "header");
    } else {
      return new ApiKey(API_KEY_NAME, properties.getHeader(), "header");
    }
  }

  private List<SecurityReference> defaultAuth() {
    AuthorizationScope[] authorizationScopes =
        new AuthorizationScope[] {new AuthorizationScope("global", "accessEverything")};

    return Collections.singletonList(new SecurityReference(API_KEY_NAME, authorizationScopes));
  }

  private List<SecurityContext> securityContexts() {
    return Collections.singletonList(
        SecurityContext.builder().securityReferences(defaultAuth()).build());
  }

  @Bean
  public AlternateTypeRuleConvention pageableConvention(
          final TypeResolver resolver) {
    return new AlternateTypeRuleConvention() {

      @Override
      public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
      }

      @Override
      public List<AlternateTypeRule> rules() {
        return Collections.singletonList(
                newRule(resolver.resolve(Pageable.class), resolver.resolve(PageableApi.class))
        );
      }
    };
  }

  @Data
  private static class PageableApi {

    @ApiParam("Results page you want to retrieve (0..N)")
    private int page;
    @ApiParam("Number of records per page")
    private int size;
    @ApiParam("Sorting criteria in the format: property(,asc|desc). "
            + "Default sort order is ascending. Multiple sort criteria are supported but can't be used via swagger.")
    private List<String> sort;
  }
}
