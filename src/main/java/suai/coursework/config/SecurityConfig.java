package suai.coursework.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import suai.coursework.config.properties.CorsProperties;
import suai.coursework.config.properties.JwtProperties;
import suai.coursework.config.properties.UrlSecurityProperties;
import suai.coursework.security.AccessTokenFilter;

@Configuration
@Profile("security")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties({CorsProperties.class, JwtProperties.class, UrlSecurityProperties.class})
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private final CorsProperties corsProperties;
  private final JwtProperties jwtProperties;
  private AccessTokenFilter accessTokenFilter;
  private final UrlSecurityProperties urlProperties;

  @Autowired
  @Lazy
  public void setAccessTokenFilter(final AccessTokenFilter accessTokenFilter) {
    this.accessTokenFilter = accessTokenFilter;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().configurationSource(corsConfigurationSource());
    http.csrf().disable();
    http.httpBasic().disable();
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.logout().deleteCookies(jwtProperties.getCookie());
    http.authorizeRequests()
        .antMatchers(urlProperties.getWhiteList())
        .permitAll()
        .antMatchers(HttpMethod.GET, urlProperties.getGetWhiteList())
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .addFilterBefore(accessTokenFilter, UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowedHeaders(corsProperties.getAllowedHeaders());
    corsConfiguration.setAllowedMethods(corsProperties.getAllowedMethods());
    corsConfiguration.setAllowedOrigins(corsProperties.getAllowedOrigins());
    corsConfiguration.setExposedHeaders(corsProperties.getExposedHeaders());

    // Действие защиты распространяется на все пути
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfiguration);
    return source;
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
