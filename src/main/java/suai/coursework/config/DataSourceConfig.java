package suai.coursework.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import suai.coursework.config.properties.MetaSqlProperties;

@Configuration
@EnableJpaRepositories({"suai.coursework.repository"})
@EntityScan({"suai.coursework.model"})
@EnableConfigurationProperties({MetaSqlProperties.class})
@EnableTransactionManagement
public class DataSourceConfig {
}
