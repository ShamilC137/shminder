package suai.coursework.config;

import java.util.List;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class CommonConfig {
  @Bean
  public ModelMapper modelMapperBean(final List<? extends Converter<?, ?>> converters) {
    final var mapper = new ModelMapper();
    for (final var converter : converters) {
      mapper.addConverter(converter);
    }
    return mapper;
  }

  @Bean
  public PasswordEncoder passwordEncoderBean() {
    return new BCryptPasswordEncoder();
  }
}
