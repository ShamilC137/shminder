package suai.coursework.config.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("spring.security")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UrlSecurityProperties {
  private String[] whiteList;
  private String[] getWhiteList;
}
