package suai.coursework.config.properties;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;

@Profile("setup")
@ConfigurationProperties("setup.sql")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class MetaSqlProperties {
  private List<String> files;
  private String directory;
  private String metaSchema;
  private String metaTable;
}
