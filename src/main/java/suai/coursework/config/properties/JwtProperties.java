package suai.coursework.config.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;

@Setter
@Getter
@NoArgsConstructor
@Profile("security")
@ConfigurationProperties("spring.security.jwt")
public class JwtProperties {
  private String header;
  private long expiration;
  private String secret;
  private String cookie;
}
