package suai.coursework.config.properties;

import java.util.List;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("spring.security.cors")
@Value
public class CorsProperties {
  private List<String> allowedHeaders;
  private List<String> allowedMethods;
  private List<String> allowedOrigins;
  private List<String> exposedHeaders;
}
