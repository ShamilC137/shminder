package suai.coursework.util.jackson;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.jayway.jsonpath.JsonPath;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.NonNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import suai.coursework.exception.InvalidDataException;

public class JsonPathArgumentResolver implements HandlerMethodArgumentResolver {

  private static final String JSONBODYATTRIBUTE = "JSON_REQUEST_BODY";

  private static final Map<Class<?>, Class<?>> PRIMITIVES_WRAP =
      Map.of(
          int.class,
          Integer.class,
          byte.class,
          Byte.class,
          char.class,
          Character.class,
          double.class,
          Double.class,
          float.class,
          Float.class,
          long.class,
          Long.class,
          short.class,
          Short.class,
          boolean.class,
          Boolean.class,
          void.class,
          Void.class);

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.hasParameterAnnotation(JsonArg.class);
  }

  @Override
  public Object resolveArgument(
      @NonNull MethodParameter parameter,
      ModelAndViewContainer mavContainer,
      @NonNull NativeWebRequest webRequest,
      WebDataBinderFactory binderFactory)
      throws Exception {
    final String body = getRequestBody(webRequest);
    final JsonArg arg = parameter.getParameterAnnotation(JsonArg.class);
    if (arg == null) {
      return null;
    }

    final String argName = getValueName(arg, parameter);
    final Object value = JsonPath.read(body, argName);
    if (value == null) {
      if (arg.required()) {
        throw new InvalidDataException(String.format("%s must be present!", argName));
      } else {
        return null;
      }
    }
    final Class<?> clazz = parameter.getParameterType();
    try {
      return tryToInstantiate(clazz, value);
    } catch (Exception exception) {
      return value;
    }
  }

  // Extracts body from given request
  private String getRequestBody(final NativeWebRequest webRequest) {
    final HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
    final String jsonBody = (String) servletRequest.getAttribute(JSONBODYATTRIBUTE);
    if (jsonBody == null) {
      try {
        final String body =
            new BufferedReader(new InputStreamReader(servletRequest.getInputStream()))
                .lines()
                .collect(Collectors.joining("\n"));
        servletRequest.setAttribute(JSONBODYATTRIBUTE, body);
        return body;
      } catch (IOException e) {
        throw new InvalidDataException("Cannot parse arguments!");
      }
    }
    return jsonBody;
  }

  /* Extracts parameter name: extracts value from JsonArg, if value is present;
  otherwise returns parameter name*/
  private String getValueName(final JsonArg arg, final MethodParameter parameter) {
    String argValue = arg.value();
    if (argValue.isEmpty()) {
      return parameter.getParameterName();
    } else {
      return argValue;
    }
  }

  // Extracts all JsonCreator methods
  private List<Method> findAnnotatedMethods(
      final Class<?> type, final Class<? extends Annotation> annotation) {
    final List<Method> methods = new ArrayList<>();
    Class<?> clazz = type;
    while (clazz != Object.class) {
      for (final Method method : clazz.getDeclaredMethods()) {
        if (method.isAnnotationPresent(annotation)) {
          methods.add(method);
        }
      }
      clazz = clazz.getSuperclass();
    }
    return methods;
  }

  // Resolves JsonCreator methods
  private Optional<Method> resolveCreators(final List<Method> creators, Object argument) {
    if (creators.isEmpty()) {
      return Optional.empty();
    }

    for (Method method : creators) {
      Parameter[] parameters = method.getParameters();
      if (parameters.length == 1) {
        Class<?> parameterClass = parameters[0].getType();
        try {
          parameterClass.cast(argument);
          return Optional.of(method);
        } catch (Exception exception) {
          if (parameterClass.isPrimitive()
              && argument.getClass().isAssignableFrom(PRIMITIVES_WRAP.get(parameterClass))) {
            return Optional.of(method);
          }
        }
      }
    }

    return Optional.empty();
  }

  // Checks that creator enabled
  private boolean creatorCanBeUsed(JsonCreator annotation) {
    return annotation.mode() != JsonCreator.Mode.DISABLED;
  }

  // Tries to create new instance using JsonCreator
  private Optional<Object> tryToUseCreators(Class<?> clazz, Object value)
      throws InvocationTargetException, IllegalAccessException {
    Optional<Method> creator =
        resolveCreators(findAnnotatedMethods(clazz, JsonCreator.class), value);
    if (creator.isPresent()) {
      Method method = creator.get();
      if (creatorCanBeUsed(method.getAnnotation(JsonCreator.class))) {
        return Optional.of(method.invoke(null, new Object[] {value}));
      }
    }
    return Optional.empty();
  }

  // Tries to create new instance using constructor, Enum.valueOf or value itself
  private Object tryToInstantiate(Class<?> clazz, Object value)
      throws NoSuchMethodException, InvocationTargetException, InstantiationException,
          IllegalAccessException {
    Optional<?> result = tryToUseCreators(clazz, value);
    if (result.isPresent()) {
      return result.get();
    }

    if (clazz.isInstance(value)) {
      return value;
    } else if (clazz.isEnum()) {
      return Enum.valueOf((Class<Enum>) clazz, (String) value);
    } else {
      return clazz.getConstructor(value.getClass()).newInstance(value);
    }
  }
}
