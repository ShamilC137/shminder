package suai.coursework.util;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MapperUtility {
  private final ModelMapper mapper;

  public <S, D> List<D> convertToDtoList(final List<S> source, final Class<D> dtoClass) {
    final List<D> dest = new ArrayList<>();
    for (final var location : source) {
      dest.add(mapper.map(location, dtoClass));
    }
    return dest;
  }
}
