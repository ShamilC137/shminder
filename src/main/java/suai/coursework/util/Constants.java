package suai.coursework.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class Constants {
  public static final String DEFAULT_PAGE = "0";
  public static final String DEFAULT_SIZE = "100";

  public static final Pageable DEFAULT_PAGEABLE = PageRequest.of(Integer.parseInt(DEFAULT_PAGE), Integer.parseInt(DEFAULT_SIZE));
}
