package suai.coursework.util.swagger;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.swagger.common.SwaggerPluginSupport;
import suai.coursework.config.properties.UrlSecurityProperties;
import suai.coursework.util.sql.utils.Pair;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
@Slf4j
@RequiredArgsConstructor
public class OperationNotesResourceReader implements OperationBuilderPlugin {
  private final DescriptionResolver resolver;
  // if method is null - open for all methods
  private final UrlSecurityProperties properties;
  private static final AntPathMatcher matcher = new AntPathMatcher();

  @Override
  public void apply(OperationContext context) {
    String note = fromAntPattern(context);

    if (note == null) {
      note = fromPreAuthorizeAnnotation(context);
    }

    if (note == null) {
      note = "User must be authorized";
    }

    context.operationBuilder().notes(resolver.resolve(note));
  }

  private String fromAntPattern(OperationContext context) {
    var resolution = resolveUrl(context.requestMappingPattern(), context.httpMethod());

    if (Boolean.TRUE.equals(resolution.getFirst())) {
      var httpMethod = resolution.getSecond();
      if (httpMethod == null) {
        return "Allowed for all users for all http methods";
      } else {
        if (httpMethod.equals(context.httpMethod())) {
          return "Allowed for all for the http method: " + httpMethod;
        } else {
          throw new RuntimeException("HttpMethod in file and HttpMethod in operation do not match");
        }
      }
    }

    return null;
  }

  private Pair<Boolean, HttpMethod> resolveUrl(String url, HttpMethod method) {
    if (method == HttpMethod.GET && match(url, properties.getGetWhiteList())) {
      return Pair.valueOf(true, HttpMethod.GET);
    }
    return Pair.valueOf(match(url, properties.getWhiteList()), null);
  }

  private boolean match(String url, String[] patterns) {
    for (var pattern : patterns) {
      if (matcher.match(pattern, url)) {
        return true;
      }
    }
    return false;
  }

  private String fromPreAuthorizeAnnotation(OperationContext context) {
    Optional<PreAuthorize> anno = context.findAnnotation(PreAuthorize.class);
    return anno.map(
            preAuthorize ->
                "Require roles: "
                    + preAuthorize.value()
                    + " for the http method: "
                    + context.httpMethod())
        .orElse(null);
  }

  @Override
  public boolean supports(DocumentationType delimiter) {
    return SwaggerPluginSupport.pluginDoesApply(delimiter);
  }
}
