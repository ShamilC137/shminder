package suai.coursework.util;

public class Authority {
  public static final String SUPPORT_ROLE = "ROLE_SUPPORT";
  public static final String USER_ROLE = "ROLE_USER";
  public static final String ADMIN_ROLE = "ROLE_ADMIN";
  public static final String MODER_ROLE = "ROLE_MODER";

  public static final String HAS_SUPPORT_ROLE = "hasRole('SUPPORT')";
  public static final String HAS_USER_ROLE = "hasRole('USER')";
  public static final String HAS_MODER_ROLE = "hasRole('MODER')";
  public static final String HAS_ADMIN_ROLE = "hasRole('ADMIN')";
}
