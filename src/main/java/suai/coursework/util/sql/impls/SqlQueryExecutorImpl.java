package suai.coursework.util.sql.impls;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import suai.coursework.util.sql.SqlQueryExecutor;
import suai.coursework.util.sql.exceptions.AcquireConnectionException;
import suai.coursework.util.sql.exceptions.CloseConnectionException;
import suai.coursework.util.sql.exceptions.InvalidQueryException;
import suai.coursework.util.sql.exceptions.StatementClosingException;
import suai.coursework.util.sql.exceptions.StatementCreationException;

@Component
public class SqlQueryExecutorImpl implements SqlQueryExecutor {
  private final Connection connection;
  private Statement statement;

  @PreDestroy
  public void closeConnection() {
    try {
      connection.close();
    } catch (SQLException e) {
      throw new CloseConnectionException("Cannot close connection", e);
    }
  }

  public SqlQueryExecutorImpl(
      @Value("${spring.datasource.url}") final String url,
      @Value("${spring.datasource.username}") final String username,
      @Value("${spring.datasource.password}") final String password)
      throws AcquireConnectionException {
    try {
      this.connection = DriverManager.getConnection(url, username, password);
    } catch (SQLException e) {
      throw new AcquireConnectionException(e);
    }
  }

  @Override
  public ResultSet execute(final String query) {
    createStatement();

    try {
      return statement.executeQuery(query);
    } catch (SQLException e) {
      throw new InvalidQueryException(e);
    }
  }

  @Override
  public int executeUpdate(final InputStreamReader stream) {
    return executeUpdate(
        new BufferedReader(stream)
            .lines()
            .map(string -> string.replaceAll("(?<=--).+", ""))
            .map(string -> string.replace("--", ""))
            .collect(Collectors.joining()));
  }

  @Override
  public int executeUpdate(final String query) {
    createStatement();
    try {
      return statement.executeUpdate(query);
    } catch (SQLException e) {
      throw new InvalidQueryException(e);
    }
  }

  /**
   * Closes underlying statement
   *
   * @throws SQLException if Statement::close throws exception
   */
  @Override
  public void close() throws SQLException {
    if (statement != null) {
      statement.close();
      statement = null;
    }
  }

  private void createStatement() {
    try {
      if (statement != null) {
        statement.close();
      }
    } catch (SQLException e) {
      throw new StatementClosingException(e);
    }

    try {
      statement = connection.createStatement();
    } catch (SQLException e) {
      throw new StatementCreationException(e);
    }
  }
}
