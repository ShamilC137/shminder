package suai.coursework.util.sql.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import suai.coursework.util.sql.SqlQueryExecutor;
import suai.coursework.util.sql.SqlSerializer;
import suai.coursework.util.sql.SqlWorker;
import suai.coursework.util.sql.exceptions.StatementClosingException;

@Component
@RequiredArgsConstructor
public class SqlWorkerImpl implements SqlWorker {
  private final SqlSerializer serializer;
  private final SqlQueryExecutor executor;

  public <T> T executeAndSerialize(final String query, final Class<T> toWhat) {
    try (executor) {
      final var set = executor.execute(query);
      return serializer.serialize(set, toWhat);
    } catch (Exception e) {
      throw new StatementClosingException(e);
    }
  }

  public <T> List<T> executeAndSerializeAll(final String query, final Class<T> toWhat) {
    try (executor) {
      final var set = executor.execute(query);
      return serializer.serializeAll(set, toWhat);
    } catch (Exception e) {
      throw new StatementClosingException(e);
    }
  }
}
