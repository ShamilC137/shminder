package suai.coursework.util.sql.impls;

import static suai.coursework.util.sql.exceptions.SetValueException.getBasicMessage;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import org.springframework.stereotype.Component;
import suai.coursework.util.sql.SqlSerializer;
import suai.coursework.util.sql.exceptions.EmptyResultSetException;
import suai.coursework.util.sql.exceptions.MissingEnumeratedException;
import suai.coursework.util.sql.exceptions.SerializationException;
import suai.coursework.util.sql.exceptions.SetValueException;
import suai.coursework.util.sql.utils.Pair;

@Component
public class SqlSerializerImpl implements SqlSerializer {
  @FunctionalInterface
  private interface ColumnValueExtractor<T> {
    T extract(final ResultSet set, final String columnName) throws SQLException;
  }

  private static final Map<Class<?>, ColumnValueExtractor<?>> EXTRACTORS =
      Map.ofEntries(
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              int.class, ResultSet::getInt),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              long.class, ResultSet::getLong),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              byte.class, ResultSet::getByte),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              short.class, ResultSet::getShort),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              float.class, ResultSet::getFloat),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              double.class, ResultSet::getDouble),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              boolean.class, ResultSet::getBoolean),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              BigDecimal.class, ResultSet::getBigDecimal),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              ZonedDateTime.class,
              (set, columnName) ->
                  ZonedDateTime.ofInstant(
                      set.getTimestamp(columnName).toInstant(), ZoneId.systemDefault())),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              Instant.class, (set, columnName) -> set.getTimestamp(columnName).toInstant()),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              LocalDate.class, (set, columnName) -> set.getDate(columnName).toLocalDate()),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              LocalTime.class, (set, columnName) -> set.getTime(columnName).toLocalTime()),
          new AbstractMap.SimpleImmutableEntry<Class<?>, ColumnValueExtractor<?>>(
              LocalDateTime.class,
              (set, columnName) -> set.getTimestamp(columnName).toLocalDateTime()));

  public <T> T serialize(final ResultSet from, final Class<T> to) {
    try {
      if (!from.next()) {
        throw new EmptyResultSetException();
      }
    } catch (SQLException e) {
      throw new SerializationException("Cannot get next value", e);
    }

    return realSerialize(from, to);
  }

  public <T> List<T> serializeAll(final ResultSet from, final Class<T> to) {
    final var result = new ArrayList<T>();
    try {
      while (from.next()) {
        result.add(realSerialize(from, to));
      }
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new SerializationException(e);
    }
    return result;
  }

  private <T> T realSerialize(final ResultSet from, final Class<T> to) {
    try {
      final var instance = to.getConstructor().newInstance();
      setFields(instance, from, to);
      return instance;
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new IllegalArgumentException("Must have default constructor!", e);
    }
  }

  private <T> void setFields(final T object, final ResultSet set, final Class<T> type) {
    for (final var pair : getFieldsAndSetters(type)) {
      final var valueType = pair.getFirst().getType();
      try {
        final var possibleExtractor = EXTRACTORS.get(valueType);
        final var field = pair.getFirst();
        final var setter = pair.getSecond();
        if (possibleExtractor == null) {
          if (valueType.isEnum()) {
            setter.invoke(object, extractEnum(set, field));
            continue;
          }

          setter.invoke(object, set.getObject(getColumnNameByField(field)));
        } else {
          setter.invoke(object, possibleExtractor.extract(set, getColumnNameByField(field)));
        }
      } catch (Exception e) {
        throw new SetValueException(getBasicMessage(pair.getFirst(), pair.getSecond()), e);
      }
    }
  }

  private <T> List<Pair<Field, Method>> getFieldsAndSetters(final Class<T> type) {
    final var result = new ArrayList<Pair<Field, Method>>();
    for (final var field : type.getDeclaredFields()) {
      if (field.getAnnotation(Transient.class) == null) {
        result.add(Pair.valueOf(field, getSetterByField(field, type)));
      }
    }
    return result;
  }

  private String getColumnNameByField(final Field field) {
    final var anno = field.getAnnotation(Column.class);
    String name;
    final String fieldName = field.getName();
    if (anno == null) {
      name = fieldNameToColumnName(fieldName);
    } else {
      name = anno.name();
    }
    return name;
  }

  private String fieldNameToColumnName(final String fieldName) {
    final StringBuilder builder = new StringBuilder();
    for (final var letter : fieldName.toCharArray()) {
      if (Character.isLowerCase(letter)) {
        builder.append(letter);
      } else {
        builder.append('_').append(Character.toLowerCase(letter));
      }
    }
    return builder.toString();
  }

  private <T> Method getSetterByField(final Field field, final Class<T> type) {
    final var methodName = fieldToSetterName(field);
    try {
      return type.getMethod(methodName, field.getType());
    } catch (Exception e) {
      throw new IllegalArgumentException("Must have setter!", e);
    }
  }

  private String fieldToSetterName(final Field field) {
    final var fieldName = field.getName();
    final StringBuilder builder = new StringBuilder("set");
    builder.append(fieldName);
    builder.setCharAt(3, Character.toUpperCase(fieldName.charAt(0)));
    return builder.toString();
  }

  private Object extractEnum(final ResultSet set, final Field field)
      throws NoSuchMethodException, SQLException, IllegalArgumentException,
          InvocationTargetException, IllegalAccessException {
    final var anno = field.getAnnotation(Enumerated.class);
    if (anno == null) {
      throw new MissingEnumeratedException();
    }

    final var columnName = getColumnNameByField(field);
    final var enumClass = field.getType();
    if (anno.value() == EnumType.STRING) {
      return enumClass.getMethod("valueOf", String.class).invoke(null, set.getString(columnName));
    } else {
      return enumClass.getEnumConstants()[set.getInt(columnName)];
    }
  }
}
