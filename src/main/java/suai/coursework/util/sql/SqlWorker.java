package suai.coursework.util.sql;

import java.util.List;

public interface SqlWorker {
  <T> T executeAndSerialize(final String query, final Class<T> toWhat);

  <T> List<T> executeAndSerializeAll(final String query, final Class<T> toWhat);
}
