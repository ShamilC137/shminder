package suai.coursework.util.sql;

import java.sql.ResultSet;
import java.util.List;

public interface SqlSerializer {
  <T> T serialize(final ResultSet from, final Class<T> to);

  <T> List<T> serializeAll(final ResultSet from, final Class<T> to);
}
