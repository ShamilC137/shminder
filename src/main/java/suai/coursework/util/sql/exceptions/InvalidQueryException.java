package suai.coursework.util.sql.exceptions;

public class InvalidQueryException extends SQLRuntimeException {
  public InvalidQueryException(final Exception cause) {
    super("Cannot execute query", cause);
  }
}
