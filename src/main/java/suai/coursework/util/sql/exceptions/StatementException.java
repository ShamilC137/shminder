package suai.coursework.util.sql.exceptions;

public class StatementException extends SQLRuntimeException {
  public StatementException() {}

  public StatementException(final String message) {
    super(message);
  }

  public StatementException(final Throwable cause) {
    super(cause);
  }

  public StatementException(final String message, final Throwable throwable) {
    super(message, throwable);
  }
}
