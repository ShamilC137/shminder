package suai.coursework.util.sql.exceptions;

public class EmptyResultSetException extends SQLRuntimeException {
  public EmptyResultSetException() {
    super("ResultSet is empty!");
  }

  public EmptyResultSetException(final Exception cause) {
    super("ResultSet is empty!", cause);
  }

  public EmptyResultSetException(final String message) {
    super(message);
  }

  public EmptyResultSetException(final String message, final Exception cause) {
    super(message, cause);
  }
}
