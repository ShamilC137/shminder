package suai.coursework.util.sql.exceptions;

public class SQLRuntimeException extends RuntimeException {
  public SQLRuntimeException() {}

  public SQLRuntimeException(final String message) {
    super(message);
  }

  public SQLRuntimeException(final Throwable cause) {
    super(cause);
  }

  public SQLRuntimeException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
