package suai.coursework.util.sql.exceptions;

public class StatementCreationException extends StatementException {
  public StatementCreationException(final Exception cause) {
    super("Cannot create statement", cause);
  }
}
