package suai.coursework.util.sql.exceptions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class SetValueException extends SQLRuntimeException {
  private static final String BASIC_MESSAGE = "Cannot set value, field: %s, setter: %s";

  public SetValueException() {
    super("Cannot set value");
  }

  public SetValueException(final String message) {
    super(message);
  }

  public SetValueException(final String message, final Exception cause) {
    super(message, cause);
  }

  public static String getBasicMessage(final Field field, final Method setter) {
    return String.format(BASIC_MESSAGE, field.getName(), setter.getName());
  }
}
