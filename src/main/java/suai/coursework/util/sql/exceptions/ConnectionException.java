package suai.coursework.util.sql.exceptions;

public class ConnectionException extends SQLRuntimeException {
  public ConnectionException(final Exception cause) {
    super("Connection exception", cause);
  }

  public ConnectionException(final String message) {
    super(message);
  }

  public ConnectionException(final String message, final Exception cause) {
    super(message, cause);
  }
}
