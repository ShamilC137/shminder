package suai.coursework.util.sql.exceptions;

public class StatementClosingException extends StatementException {
  public StatementClosingException(final Throwable cause) {
    super("Cannot close statement", cause);
  }
}
