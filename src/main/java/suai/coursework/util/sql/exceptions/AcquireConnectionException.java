package suai.coursework.util.sql.exceptions;

public class AcquireConnectionException extends SQLRuntimeException {
  public AcquireConnectionException() {
    super("Cannot acquire connection");
  }

  public AcquireConnectionException(final Exception cause) {
    super("Cannot acquire connection", cause);
  }

  public AcquireConnectionException(final String message) {
    super(message);
  }

  public AcquireConnectionException(final String message, final Exception cause) {
    super(message, cause);
  }
}
