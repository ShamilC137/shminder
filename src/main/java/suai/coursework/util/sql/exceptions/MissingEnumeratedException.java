package suai.coursework.util.sql.exceptions;

public class MissingEnumeratedException extends SQLRuntimeException {
  public MissingEnumeratedException() {
    super("All enumerated field must contain @Enumerated!");
  }

  public MissingEnumeratedException(final String message) {
    super(message);
  }

  public MissingEnumeratedException(final String message, final Exception cause) {
    super(message, cause);
  }
}
