package suai.coursework.util.sql.exceptions;

public class CloseConnectionException extends ConnectionException {
  public CloseConnectionException() {
    super("Cannot close connection");
  }

  public CloseConnectionException(final Exception cause) {
    super("Cannot close connection", cause);
  }

  public CloseConnectionException(final String message) {
    super(message);
  }

  public CloseConnectionException(final String message, final Exception cause) {
    super(message, cause);
  }
}
