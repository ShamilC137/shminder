package suai.coursework.util.sql.exceptions;

public class SerializationException extends SQLRuntimeException {
  public SerializationException() {
    super("Cannot serialize value");
  }

  public SerializationException(final Exception cause) {
    super("Cannot serialize value", cause);
  }

  public SerializationException(final String message) {
    super(message);
  }

  public SerializationException(final String message, final Exception cause) {
    super(message, cause);
  }
}
