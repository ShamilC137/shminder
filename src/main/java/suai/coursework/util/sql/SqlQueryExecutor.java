package suai.coursework.util.sql;

import java.io.InputStreamReader;
import java.sql.ResultSet;

public interface SqlQueryExecutor extends AutoCloseable {
  ResultSet execute(final String query);

  int executeUpdate(final InputStreamReader stream);

  int executeUpdate(final String query);
}
