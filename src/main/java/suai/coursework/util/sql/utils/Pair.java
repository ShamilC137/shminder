package suai.coursework.util.sql.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pair<F, S> {
  private F first;
  private S second;

  public static <T, U> Pair<T, U> valueOf(final T first, final U second) {
    return new Pair<>(first, second);
  }
}
