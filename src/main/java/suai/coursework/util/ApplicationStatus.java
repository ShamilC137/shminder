package suai.coursework.util;

public enum ApplicationStatus {
  NEW,
  APPROVED,
  DISAPPROVED, // закрытая администратором
  CLOSED // закрытая явно пользователем
}
