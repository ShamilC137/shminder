package suai.coursework.service.impls;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Image;
import suai.coursework.repository.ImageRepo;
import suai.coursework.service.ImageService;

@Service
@RequiredArgsConstructor
@Transactional
public class ImageServiceImpl implements ImageService {
  private final ImageRepo imageRepo;

  @Override
  public Image save(final Image image) {
    return imageRepo.save(image);
  }

  @Override
  public Image getById(final int userId) {
    return imageRepo.findById(userId).orElseThrow(() -> new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "Image", String.valueOf(userId))));
  }
}
