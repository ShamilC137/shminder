package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.AppUser;
import suai.coursework.model.Like;
import suai.coursework.repository.LikeRepo;
import suai.coursework.service.LikeService;

@Service
@RequiredArgsConstructor
@Transactional
public class LikeServiceImpl implements LikeService {
  private final LikeRepo likeRepo;

  @Override
  public List<AppUser> findAllByUserLikes(final int userId, final Pageable pageable) {
    return likeRepo.findAppUserByInitiatorId(userId, pageable);
  }

  @Override
  public List<AppUser> findAllToUserLikes(final int userId, final Pageable pageable) {
    return likeRepo.findAppUserByVictimId(userId, pageable);
  }

  @Override
  public Like save(final Like like) {
    try {
      Like result = likeRepo.save(like);
      likeRepo.flush();
      return result;
    } catch (DataAccessException e) {
      throw new AlreadyExistException(String.format(AlreadyExistException.BASIC_MESSAGE, "Like", "key"));
    }
  }

  @Override
  public void remove(final Like.LikeKey key) {
    try {
      likeRepo.delete(new Like(key));
      likeRepo.flush();
    } catch (DataAccessException e) {
      throw new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "Like", "key"));
    }
  }
}
