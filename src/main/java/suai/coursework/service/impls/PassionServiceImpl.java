package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Passion;
import suai.coursework.repository.PassionRepo;
import suai.coursework.service.PassionService;

@Service
@Transactional
@RequiredArgsConstructor
public class PassionServiceImpl implements PassionService {
  private final PassionRepo passionRepo;

  @Override
  @Transactional(readOnly = true)
  public Passion getById(final int id) {
    return passionRepo
        .findById(id)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Passion", "id")));
  }

  @Override
  public Passion getByName(final String name) {
    return passionRepo
        .findByNameIgnoreCase(name)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Passion", "name")));
  }

  @Override
  public List<Passion> findAll(final Pageable pageable) {
    return passionRepo.findAll(pageable).getContent();
  }

  @Override
  public Passion updateOrSave(final Passion data) {
    return passionRepo.save(data);
  }

  @Override
  public Passion save(final Passion data) {
    passionRepo
        .findByNameIgnoreCase(data.getName())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, "Passion", "name"));
            });
    return passionRepo.save(data);
  }
}
