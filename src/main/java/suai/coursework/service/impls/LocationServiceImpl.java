package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Country;
import suai.coursework.model.Location;
import suai.coursework.repository.LocationRepo;
import suai.coursework.service.CountryService;
import suai.coursework.service.LocationService;

@Service
@Transactional
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {
  private final LocationRepo locationRepo;
  private final CountryService countryService;

  @Override
  public Location getById(final int id) {
    return locationRepo
        .findById(id)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Locaiton", "id")));
  }

  @Override
  public Location getByCity(final String city) {
    return locationRepo
        .findByCityIgnoreCase(city)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Locaiton", "city")));
  }

  @Override
  public Location save(Location data) {
    locationRepo
        .findByCityIgnoreCase(data.getCity())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, "Location", "city"));
            });
    return locationRepo.save(data);
  }

  @Override
  public Location updateOrSave(Location data) {
    return locationRepo.save(data);
  }

  @Override
  public List<Location> findAll(final Pageable pageable) {
    return locationRepo.findAll(pageable).getContent();
  }

  @Override
  public List<Location> findAllByCountryName(final String name, final Pageable pageable) {
    final Country country = countryService.getByName(name);
    return locationRepo.findAllByCountryId(country.getId(), pageable);
  }

  @Override
  public List<Location> findAllByCityStartsWith(final String name, final Pageable pageable) {
    return locationRepo.findAllByCityStartsWithIgnoreCase(name, pageable);
  }

  @Override
  public List<Location> findAllByCityAndCountry(final String countryName,
                                                final String cityPrefix,
                                                final Pageable pageable) {
    return locationRepo.findAllByCountryIdAndCityStartingWithIgnoreCase(
        countryService.getByName(countryName).getId(), cityPrefix, pageable);
  }
}
