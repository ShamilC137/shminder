package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Role;
import suai.coursework.repository.RoleRepo;
import suai.coursework.service.RoleService;

@Service
@Transactional
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
  private final RoleRepo roleRepo;

  @Override
  @Transactional(readOnly = true)
  public Role getById(final int id) {
    return roleRepo
        .findById(id)
        .orElseThrow(
            () ->
                new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "Role", "id")));
  }

  @Override
  @Transactional(readOnly = true)
  public List<Role> findAll() {
    return roleRepo.findAll();
  }

  @Override
  public Role save(Role role) {
    roleRepo
        .findByNameIgnoreCase(role.getName())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, "Role", "id"));
            });

    return roleRepo.save(role);
  }

  @Override
  @Transactional(readOnly = true)
  public Role getByName(final String name) {
    return roleRepo
        .findByNameIgnoreCase(name)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Role", "name")));
  }

  @Override
  public Role updateOrSave(final Role role) {
    return roleRepo.save(role);
  }
}
