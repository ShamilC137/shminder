package suai.coursework.service.impls;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.AppUser;
import suai.coursework.model.UserPassion;
import suai.coursework.model.UserRole;
import suai.coursework.repository.UserRepo;
import suai.coursework.service.UserPassionService;
import suai.coursework.service.UserRoleService;
import suai.coursework.service.UserService;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {
  private final UserRepo userRepo;
  private final UserRoleService userRoleService;
  private final PasswordEncoder passwordEncoder;
  private final UserPassionService userPassionService;

  @Override
  public AppUser getByEmail(String email) {
    return initUser(
        userRepo
            .findAppUserByEmailIgnoreCase(email)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        String.format(NotFoundException.BASIC_MESSAGE, "User", "email"))));
  }

  @Override
  public AppUser save(final AppUser data) {
    userRepo
        .findAppUserByEmailIgnoreCase(data.getEmail())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, "User", "email"));
            });
    data.setPassword(passwordEncoder.encode(data.getPassword()));
    AppUser result = userRepo.save(data);
    final int userId = result.getId();
    userRoleService.save(
        new UserRole(
            UserRole.UserRoleKey.builder()
                .userId(userId)
                .roleId(result.getRoles().get(0).getId())
                .build()));
    data.getPassions()
        .forEach(
            passion ->
                userPassionService.save(
                    new UserPassion(new UserPassion.UserPassionKey(userId, passion.getId()))));
    return result;
  }

  @Override
  @Transactional(readOnly = true)
  public AppUser getById(final int id) {
    return initUser(
        userRepo
            .findById(id)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        String.format(NotFoundException.BASIC_MESSAGE, "User", "id"))));
  }

  @Override
  public AppUser updateOrSave(final AppUser data) {
    return userRepo.save(data);
  }

  @Override
  public int setUserEnabledStatus(final int id, final boolean enabled) {
    final int result = userRepo.setEnableStatusById(id, enabled);
    if (result != 1) {
      throw new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "User", "id"));
    }
    return result;
  }

  private AppUser initUser(final AppUser user) {
    final int userId = user.getId();
    user.setRoles(userRoleService.findAllByUserId(userId));
    user.setPassions(userPassionService.findPassionsByUserId(userId, PageRequest.of(0, Integer.MAX_VALUE)));
    return user;
  }
}
