package suai.coursework.service.impls;

import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.AppUser;
import suai.coursework.model.Passion;
import suai.coursework.model.UserPassion;
import suai.coursework.repository.UserPassionRepo;
import suai.coursework.service.UserPassionService;
import suai.coursework.util.Gender;
import suai.coursework.util.sql.SqlWorker;

@NoArgsConstructor(access = AccessLevel.NONE)
class Queries {
  public static final String FIND_ALL_BY_PREFERENCE =
      "SELECT u.* \n"
          + "FROM coursework.users u \n"
          + "   INNER JOIN\n"
          + "   (\n"
          + "     SELECT u.id, COUNT(up.fk_passion_id) AS total, array_agg(up.fk_passion_id) AS passions \n"
          + "     FROM coursework.users u INNER JOIN coursework.users_passions up ON u.id = up.fk_user_id \n"
          + "     WHERE up.fk_passion_id IN \n"
          + "       ( \n"
          + "         SELECT fk_passion_id \n"
          + "         FROM coursework.users_passions \n"
          + "         WHERE fk_user_id = %d\n"
          + "       ) \n"
          + "   GROUP BY u.id\n"
          + "   ) AS tmp ON u.id = tmp.id \n"
          + "WHERE u.id != %d AND u.gender = '%s' \n"
          + "ORDER BY total DESC \n"
          + "LIMIT %d OFFSET %d\n";
}

@Service
@Transactional
@RequiredArgsConstructor
public class UserPassionServiceImpl implements UserPassionService {
  private final UserPassionRepo userPassionRepo;
  private final SqlWorker worker;

  @Override
  public List<Passion> findPassionsByUserId(int userId, final Pageable pageable) {
    return userPassionRepo.findPassionsByUserId(userId, pageable);
  }

  @Override
  public List<AppUser> findUsersByPassionIdAndGender(
          int passionId, final Gender gender, final Pageable pageable) {
    return userPassionRepo.findUsersByPassionIdAndGender(
        passionId, gender, pageable);
  }

  @Override
  public UserPassion save(UserPassion userPassion) {
    try {
      UserPassion result = userPassionRepo.save(userPassion);
      userPassionRepo.flush();
      return result;
    } catch (DataAccessException e) {
      throw new AlreadyExistException(
          String.format(AlreadyExistException.BASIC_MESSAGE, "UserPassion", "key"));
    }
  }

  @Override
  public List<AppUser> findByPreferences(
      final int userId, final String gender, final int page, final int size) {
    return worker.executeAndSerializeAll(
        String.format(Queries.FIND_ALL_BY_PREFERENCE, userId, userId, gender, size, page),
        AppUser.class);
  }

  @Override
  public void delete(final UserPassion userPassion) {
    try {
      userPassionRepo.delete(userPassion);
    } catch (DataAccessException e) {
      throw new NotFoundException(String.format(NotFoundException.BASIC_MESSAGE, "UserPassion", "data"), e);
    }
  }
}
