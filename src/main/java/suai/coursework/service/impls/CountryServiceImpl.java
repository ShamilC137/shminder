package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Country;
import suai.coursework.repository.CountryRepo;
import suai.coursework.service.CountryService;

@Service
@RequiredArgsConstructor
@Transactional
public class CountryServiceImpl implements CountryService {
  private final CountryRepo countryRepo;

  private static final String ENTITY_NAME = "Country";

  @Override
  public Country getById(final int id) {
    return countryRepo
        .findById(id)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, ENTITY_NAME, "id")));
  }

  @Override
  public Country getByName(final String name) {
    return countryRepo
        .findByNameIgnoreCase(name)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, ENTITY_NAME, "name")));
  }

  @Override
  public List<Country> findLikeName(final String name, final Pageable pageable) {
    return countryRepo.findByNameStartsWithIgnoreCase(name, pageable);
  }

  @Override
  public Country save(final Country data) {
    countryRepo
        .findByNameIgnoreCase(data.getName())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, ENTITY_NAME, "name"));
            });
    return countryRepo.save(data);
  }

  @Override
  public Country updateOrSave(final Country data) {
    return countryRepo.save(data);
  }

  @Override
  public List<Country> findAll(final Pageable pageable) {
    return countryRepo.findAll(pageable).getContent();
  }
}
