package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.dto.data.CountryPassionStatisticsDataDto;
import suai.coursework.service.StatisticsService;
import suai.coursework.util.sql.SqlWorker;

@Component
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {
  private final SqlWorker worker;

  private static class Queries {
    public static final String FIND_ALL_COUNTRY_PASSIONS =
        "SELECT c.name country_name, p.name passion_name, COUNT(*) users_count\n"
            + "FROM coursework.users u \n"
            + "\tINNER JOIN coursework.locations l ON u.fk_location_id = l.id\n"
            + "\tINNER JOIN coursework.users_passions up ON u.id = up.fk_user_id\n"
            + "\tINNER JOIN coursework.passions p ON up.fk_passion_id = p.id\n"
            + "\tINNER JOIN coursework.countries c ON l.fk_country_id = c.id\n"
            + "GROUP BY c.name, p.name\n"
            + "ORDER BY users_count DESC\n"
            + "LIMIT %d OFFSET %d;";

    public static final String FIND_MAX_COUNTRY_PASSIONS =
        "SELECT c.name country_name, p.name passion_name, users_count\n"
            + "FROM\n"
            + "(\n"
            + "\tSELECT cid, pid, users_count, MIN(pid) OVER (PARTITION BY cid) min_pid\n"
            + "\tFROM\n"
            + "\t(\n"
            + "\t\tSELECT *, MAX(users_count) OVER (PARTITION BY cid) max_users_count\n"
            + "\t\tFROM\n"
            + "\t\t(\n"
            + "\t\t\tSELECT l.fk_country_id cid, up.fk_passion_id pid, COUNT(*) users_count\n"
            + "\t\t\tFROM coursework.users u \n"
            + "\t\t\t\tINNER JOIN coursework.locations l ON u.fk_location_id = l.id\n"
            + "\t\t\t\tINNER JOIN coursework.users_passions up ON u.id = up.fk_user_id\n"
            + "\t\t\tGROUP BY l.fk_country_id, up.fk_passion_id\n"
            + "\t\t) tmp\n"
            + "\t) tmp\n"
            + "\tWHERE max_users_count = users_count\n"
            + ") tmp\n"
            + "\tINNER JOIN coursework.countries c ON tmp.cid = c.id\n"
            + "\tINNER JOIN coursework.passions p ON tmp.pid = p.id\n"
            + "WHERE pid = min_pid\n"
            + "ORDER BY users_count DESC\n"
            + "LIMIT %d OFFSET %d;";
  }

  @Override
  @Transactional(readOnly = true)
  public List<CountryPassionStatisticsDataDto> findAllCountriesAndPassionsStatistics(
      int page, int size) {
    return worker.executeAndSerializeAll(
        String.format(Queries.FIND_ALL_COUNTRY_PASSIONS, size, page),
        CountryPassionStatisticsDataDto.class);
  }

  @Override
  @Transactional(readOnly = true)
  public List<CountryPassionStatisticsDataDto> findCountriesWithMaxPassionUsers(
      int page, int size) {
    return worker.executeAndSerializeAll(
        String.format(Queries.FIND_MAX_COUNTRY_PASSIONS, size, page),
        CountryPassionStatisticsDataDto.class);
  }
}
