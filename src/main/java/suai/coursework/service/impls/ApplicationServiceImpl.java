package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.AppUser;
import suai.coursework.model.Application;
import suai.coursework.model.Role;
import suai.coursework.model.UserRole;
import suai.coursework.repository.ApplicationRepo;
import suai.coursework.service.ApplicationService;
import suai.coursework.service.RoleService;
import suai.coursework.service.UserRoleService;
import suai.coursework.service.UserService;
import suai.coursework.util.ApplicationStatus;

@Service
@Transactional
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {
  private final ApplicationRepo applicationRepo;
  private final UserService userService;
  private final RoleService roleService;
  private final UserRoleService userRoleService;

  @Override
  @Transactional(readOnly = true)
  public Application getById(final int id) {
    return applicationRepo
        .findById(id)
        .orElseThrow(
            () ->
                new NotFoundException(
                    String.format(NotFoundException.BASIC_MESSAGE, "Application", "id")));
  }

  @Override
  @Transactional(readOnly = true)
  public List<Application> findByUserId(final int userId, final Pageable pageable) {
    return applicationRepo.findByUserId(userId, pageable);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Application> findByStatus(
      final ApplicationStatus status, final Pageable pageable) {
    return applicationRepo.findByStatus(status, pageable);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Application> findByUserIdAndStatus(
      final int userId, final ApplicationStatus status, final Pageable pageable) {
    return applicationRepo.findByUserIdAndStatus(userId, status, pageable);
  }

  @Override
  public Application save(final Application data) {
    userService.getById(data.getUserId()).getRoles().stream()
        .filter(role -> role.getId().equals(data.getDesiredRoleId()))
        .findAny()
        .ifPresent(
            val -> {
              throw new AlreadyExistException("User already has such role!");
            });

    if (!applicationRepo
        .findByUserIdAndStatus(
            data.getUserId(), ApplicationStatus.NEW, PageRequest.of(0, Integer.MAX_VALUE))
        .isEmpty()) {
      throw new AlreadyExistException("User can have only one active application with role!");
    }
    return applicationRepo.save(data);
  }

  @Override
  public int changeApplicationStatus(int applicationId, ApplicationStatus newStatus) {
    final Application application = getById(applicationId);
    final Role role = roleService.getById(application.getDesiredRoleId());
    final AppUser user = userService.getById(application.getUserId());
    final UserRole userRole =
        UserRole.builder()
            .key(UserRole.UserRoleKey.builder().userId(user.getId()).roleId(role.getId()).build())
            .build();
    if (newStatus == ApplicationStatus.APPROVED) {
      userRoleService.save(userRole);
    } else {
      userRoleService.remove(userRole);
    }

    return applicationRepo.changeApplicationStatus(applicationId, newStatus);
  }

  @Override
  public List<Application> findAll(final Pageable pageable) {
    return applicationRepo.findAll(pageable).getContent();
  }
}
