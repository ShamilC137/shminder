package suai.coursework.service.impls;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import suai.coursework.exception.AlreadyExistException;
import suai.coursework.exception.NotFoundException;
import suai.coursework.model.Role;
import suai.coursework.model.UserRole;
import suai.coursework.repository.UserRoleRepo;
import suai.coursework.service.UserRoleService;

@Component
@Transactional
@RequiredArgsConstructor
public class UserRoleServiceImpl implements UserRoleService {
  private final UserRoleRepo userRoleRepo;

  @Override
  public UserRole save(final UserRole userRole) {
    userRoleRepo
        .findById(userRole.getKey())
        .ifPresent(
            val -> {
              throw new AlreadyExistException(
                  String.format(AlreadyExistException.BASIC_MESSAGE, "UserRole", "key"));
            });
    return userRoleRepo.save(userRole);
  }

  @Override
  public List<Role> findAllByUserId(final int userId) {
    return userRoleRepo.findAllByUserId(userId);
  }

  @Override
  public void remove(final UserRole userRole) {
    try {
      userRoleRepo.delete(userRole);
    } catch (DataAccessException e) {
      throw new NotFoundException(
              String.format(NotFoundException.BASIC_MESSAGE, "UserRole", "key"));
    }
  }
}
