package suai.coursework.service.impls;

import java.util.Arrays;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import suai.coursework.config.properties.JwtProperties;
import suai.coursework.security.AccessTokenProvider;
import suai.coursework.security.SecurityUser;
import suai.coursework.service.AuthService;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
  private final AuthenticationManager manager;
  private final AccessTokenProvider tokenProvider;
  private final JwtProperties jwtProperties;

  public SecurityUser login(
      final String username, final String password, final HttpServletResponse response) {
    Authentication auth =
        manager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    applyToken(response, createToken(auth));
    return (SecurityUser) auth.getPrincipal();
  }

  private String createToken(final Authentication authentication) {
    return tokenProvider.createToken((SecurityUser) authentication.getPrincipal());
  }

  private void applyToken(final HttpServletResponse response, final String token) {
    setTokenToCookie(response, token, (int) jwtProperties.getExpiration());
    response.addHeader(jwtProperties.getHeader(), token);
  }

  private void setTokenToCookie(
      final HttpServletResponse response, final String token, final int maxAge) {
    Cookie cookie = new Cookie(jwtProperties.getCookie(), token);
    cookie.setSecure(true);
    cookie.setHttpOnly(true);
    cookie.setPath("/");
    cookie.setMaxAge(maxAge);
    response.addCookie(cookie);
  }

  public void logout(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Authentication authentication) {
    SecurityContextLogoutHandler handler = new SecurityContextLogoutHandler();
    handler.logout(request, response, authentication);

    final var cookieName = jwtProperties.getCookie();
    final var cookies = request.getCookies();
    if (cookies != null) {
      Arrays.stream(cookies)
          .filter(cookie -> cookieName.equals(cookie.getName()))
          .findAny()
          .ifPresent(cookie -> setTokenToCookie(response, cookie.getName(), 0));
    }
  }
}
