package suai.coursework.service;

import suai.coursework.model.Image;

public interface ImageService {
  Image save(final Image image);

  Image getById(final int userId);
}
