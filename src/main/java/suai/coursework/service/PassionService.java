package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.Passion;

public interface PassionService {
  Passion getById(final int id);

  Passion getByName(final String name);

  List<Passion> findAll(final Pageable pageable);

  Passion updateOrSave(final Passion data);

  Passion save(final Passion data);
}
