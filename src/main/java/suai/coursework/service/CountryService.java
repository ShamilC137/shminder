package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.Country;

public interface CountryService {
  Country getById(final int id);

  Country getByName(final String name);

  List<Country> findLikeName(final String name, final Pageable pageable);

  Country save(final Country data);

  Country updateOrSave(final Country data);

  List<Country> findAll(final Pageable pageable);
}
