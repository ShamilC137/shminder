package suai.coursework.service;

import java.util.List;
import suai.coursework.model.Role;
import suai.coursework.model.UserRole;

public interface UserRoleService {
  UserRole save(final UserRole userRole);

  void remove(final UserRole userRole);

  List<Role> findAllByUserId(final int userId);
}
