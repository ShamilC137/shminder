package suai.coursework.service;

import java.util.List;
import suai.coursework.model.Role;

public interface RoleService {
  Role getById(int id);

  List<Role> findAll();

  Role save(Role role);

  Role getByName(String name);

  Role updateOrSave(Role role);
}
