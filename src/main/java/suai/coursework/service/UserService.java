package suai.coursework.service;

import suai.coursework.model.AppUser;

public interface UserService {
  AppUser getByEmail(final String email);

  AppUser save(final AppUser data);

  AppUser getById(final int id);

  AppUser updateOrSave(final AppUser data);

  int setUserEnabledStatus(final int id, final boolean enabled);
}
