package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.Location;

public interface LocationService {
  Location getById(final int id);

  Location getByCity(final String name);

  Location save(final Location data);

  Location updateOrSave(final Location data);

  List<Location> findAll(final Pageable pageable);

  List<Location> findAllByCountryName(final String name, final Pageable pageable);

  List<Location> findAllByCityStartsWith(final String name, final Pageable pageable);

  List<Location> findAllByCityAndCountry(
      final String cityPrefix, final String countryName, final Pageable pageable);
}
