package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.AppUser;
import suai.coursework.model.Like;

public interface LikeService {
  List<AppUser> findAllByUserLikes(final int userId, final Pageable pageable);

  List<AppUser> findAllToUserLikes(final int userId, final Pageable pageable);

  void remove(final Like.LikeKey key);

  Like save(final Like like);
}
