package suai.coursework.service;

import java.util.List;
import suai.coursework.dto.data.CountryPassionStatisticsDataDto;

public interface StatisticsService {
  List<CountryPassionStatisticsDataDto> findAllCountriesAndPassionsStatistics(
      final int page, final int size);

  List<CountryPassionStatisticsDataDto> findCountriesWithMaxPassionUsers(
      final int page, final int size);
}
