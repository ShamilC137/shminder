package suai.coursework.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import suai.coursework.security.SecurityUser;

public interface AuthService {
  SecurityUser login(
      final String username, final String password, final HttpServletResponse response);

  void logout(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Authentication authentication);
}
