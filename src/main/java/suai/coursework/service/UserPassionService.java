package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.AppUser;
import suai.coursework.model.Passion;
import suai.coursework.model.UserPassion;
import suai.coursework.util.Gender;

public interface UserPassionService {
  List<Passion> findPassionsByUserId(final int userId, final Pageable pageable);

  List<AppUser> findUsersByPassionIdAndGender(final int passionId, final Gender gender, final Pageable pageable);

  UserPassion save(final UserPassion userPassion);

  List<AppUser> findByPreferences(
      final int userId, final String gender, final int page, final int size);

  void delete(final UserPassion userPassion);
}
