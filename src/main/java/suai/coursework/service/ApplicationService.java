package suai.coursework.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import suai.coursework.model.Application;
import suai.coursework.util.ApplicationStatus;

public interface ApplicationService {
  Application getById(final int id);

  List<Application> findByUserId(final int userId, final Pageable pageable);

  List<Application> findByStatus(final ApplicationStatus status, final Pageable pageable);

  Application save(final Application data);

  int changeApplicationStatus(final int applicationId, final ApplicationStatus newStatus);

  List<Application> findAll(final Pageable pageable);

  List<Application> findByUserIdAndStatus(
      final int userId, final ApplicationStatus status, final Pageable pageable);
}
