package suai.coursework.dto.data;

import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.dto.groups.UpdateGroup;
import suai.coursework.validation.SatisfyRegexpValidation;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PassionDataDto {
  @Min(value = 1, message = "Cannot be lesser than one", groups = UpdateGroup.class)
  private int id;

  @SatisfyRegexpValidation(regexp = "^[a-zA-Zа-яА-Я-\\s]+$", groups = UpdateGroup.class)
  private String name;
}
