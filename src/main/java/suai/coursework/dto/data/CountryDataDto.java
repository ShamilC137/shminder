package suai.coursework.dto.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.dto.groups.UpdateGroup;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryDataDto {
  @Min(groups = UpdateGroup.class, value = 1, message = "Cannot be lesser than one")
  private int id;

  @NotBlank(groups = UpdateGroup.class, message = "Cannot be blank")
  private String name;
  @Min(value = 1, groups = UpdateGroup.class, message = "Cannot be lesser than one")
  private Integer countryCode;
}
