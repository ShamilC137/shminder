package suai.coursework.dto.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class LocationWithStatisticsDataDto {
  private String city;
  private int totalUsersCount;
}
