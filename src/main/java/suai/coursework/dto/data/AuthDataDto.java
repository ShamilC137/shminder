package suai.coursework.dto.data;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.validation.PasswordConstraint;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AuthDataDto {
  @NotNull
  private String username;
  @NotNull
  @PasswordConstraint
  private String password;
}
