package suai.coursework.dto.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CountryWithStatisticsDataDto {
  private String name;
  private int totalLocationsCount;
}
