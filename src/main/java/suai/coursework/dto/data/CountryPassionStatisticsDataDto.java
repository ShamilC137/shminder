package suai.coursework.dto.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CountryPassionStatisticsDataDto {
  private String countryName;
  private String passionName;
  private int usersCount;
}
