package suai.coursework.dto.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.model.Country;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocationDataDto {
  private int id;
  private Country country;
  private String city;
}
