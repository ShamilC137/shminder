package suai.coursework.dto.data;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.model.Location;
import suai.coursework.model.Passion;
import suai.coursework.model.Role;
import suai.coursework.util.Gender;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDataDto {
  private int id;
  private String firstName;
  private String lastName;
  private String middleName;
  private LocalDate dateOfBirth;
  private String email;
  private long phone;
  private List<Passion> passions;
  private List<Role> roles;
  private Location location;
  private String about;
  private Gender gender;
}
