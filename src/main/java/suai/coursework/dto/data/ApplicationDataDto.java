package suai.coursework.dto.data;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.util.ApplicationStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDataDto {
  private int id;
  private ZonedDateTime createdAt;
  private ApplicationStatus status;
  private int desiredRoleId;
  private int userId;
}
