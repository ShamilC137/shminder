package suai.coursework.dto.update;

import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocationUpdateDto {
  @Min(value = 1, message = "Cannot be lesser than one")
  private int id;
  @Min(value = 1, message = "Cannot be lesser than one")
  private Integer countryId;
  private String city;
}
