package suai.coursework.dto.create;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import suai.coursework.util.Gender;
import suai.coursework.validation.CollectionLengthConstraint;
import suai.coursework.validation.PasswordConstraint;
import suai.coursework.validation.SatisfyRegexpValidation;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserCreateDto {
  @NotBlank
  @SatisfyRegexpValidation(regexp = "^[a-zA-Zа-яА-Я]+$")
  private String firstName;

  @NotBlank
  @SatisfyRegexpValidation(regexp = "^[a-zA-Zа-яА-Я]+$")
  private String lastName;

  @SatisfyRegexpValidation(regexp = "^[a-zA-Zа-яА-Я]+$", nullable = true)
  private String middleName;

  @NotBlank @Email private String email;

  @NotBlank
  @PasswordConstraint
  private String password;

  @NotNull
  @JsonProperty(required = true)
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateOfBirth;

  @Min(1000000000L)
  @Max(9999999999L)
  @JsonProperty(required = true)
  private long phone;

  @NotNull
  @CollectionLengthConstraint(type = CollectionLengthConstraint.Type.GREATER_OR_EQUAL, value = 1)
  private List<Integer> passionsIds;

  private int locationId;

  private Gender gender;

  private String about;
}
