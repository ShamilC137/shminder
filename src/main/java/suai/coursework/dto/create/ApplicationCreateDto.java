package suai.coursework.dto.create;

import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationCreateDto {
  @Min(value = 1, message = "Cannot be lesser than one")
  private int desiredRoleId;
  @Min(value = 1, message = "Cannot be lesser than one")
  private int userId;
}
