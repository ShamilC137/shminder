TRUNCATE coursework.applications RESTART IDENTITY CASCADE;

INSERT INTO coursework.applications (fk_user_id, fk_desired_role_id, status)
VALUES (50, 4, 3),
       (7, 2, 1),
       (31, 3, 1),
       (11, 2, 0),
       (11, 2, 3),
       (29, 1, 1),
       (14, 3, 3),
       (11, 3, 3),
       (38, 1, 1),
       (10, 1, 1),
       (32, 3, 1),
       (16, 4, 2),
       (43, 3, 1),
       (5, 1, 1),
       (20, 3, 0)
ON CONFLICT DO NOTHING;
