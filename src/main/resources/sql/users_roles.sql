INSERT INTO coursework.users_roles(fk_user_id, fk_role_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1),
       (8, 1),
       (9, 1),
       (10, 1),
       (11, 1),
       (12, 1),
       (13, 1),
       (14, 1),
       (15, 1),
       (16, 1),
       (17, 1),
       (18, 1),
       (19, 1),
       (20, 1),
       (21, 1),
       (22, 1),
       (23, 1),
       (24, 1),
       (25, 1),
       (26, 1),
       (27, 1),
       (28, 1),
       (29, 1),
       (30, 1),
       (31, 1),
       (32, 1),
       (33, 1),
       (34, 1),
       (35, 1),
       (36, 1),
       (37, 1),
       (38, 1),
       (39, 1),
       (40, 1),
       (41, 1),
       (42, 1),
       (43, 1),
       (44, 1),
       (45, 1),
       (46, 1),
       (47, 1),
       (48, 1),
       (49, 1),
       (50, 1),
       (51, 1),
       (25, 4),
       (3, 4),
       (14, 3),
       (33, 4),
       (32, 4),
       (36, 3),
       (38, 3),
       (18, 4),
       (30, 3),
       (14, 2),
       (21, 4),
       (7, 3),
       (43, 4),
       (18, 2),
       (41, 4),
       (45, 2),
       (44, 3),
       (42, 4),
       (26, 3),
       (13, 4),
       (16, 4),
       (33, 3),
       (8, 4),
       (41, 3),
       (26, 2),
       (28, 2),
       (20, 3),
       (34, 3),
       (29, 2),
       (26, 4),
       (30, 4),
       (32, 3),
       (49, 4),
       (15, 2),
       (46, 4),
       (42, 3),
       (1, 2),
       (1, 3),
       (1, 4)
ON CONFLICT DO NOTHING;