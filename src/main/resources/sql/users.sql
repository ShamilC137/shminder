TRUNCATE coursework.users RESTART IDENTITY CASCADE;

INSERT INTO coursework.users(fk_location_id, first_name, last_name, middle_name, email,
                             password, phone, gender, date_of_birth, about, created_at)
VALUES (50479, 'Shamil', 'Mukhetdinov', 'Ramilevich', 'shmukhetdinov@gmail.com',
        '$2a$10$nfVQhbJ9QdlAysjc6hRuEuZuamumFstc2vMW26N89r1NJ9UJNvBe6',
        9228127198, 'MALE', '2001-01-26', 'Student', NOW()),            -- Aaaaaa123
       (19787, 'Ethelda', 'Stooke', 'Brian', 'bstooke0@thetimes.co.uk',
        '$2a$10$oFQvOGjVcdQCJw7gnEmAc.j4IKdDGI8lTyKNfD/N4d12Lgei/lOSO', 8813798534,
        'ATTACKHELICOPTER', '1998-05-03', 'FUCK OUT', now()),           --NeGIXjT9
       (20622, 'Ferrel', 'Whitear', 'Marcelle', 'mwhitear1@furl.net',
        '$2a$10$97tcF5wMN0v5ZrMasDQxMO6KJp2rQfLQffYoVUBXypyVmJ5vFWKEy', 4081076625, 'FEMALE',
        '1997-12-12',
        NULL, now()),                                                   --70WtiZO5
       (48872, 'Cooper', 'Ledbetter', 'Mignon', 'mledbetter2@noaa.gov',
        '$2a$10$InTMn/md0Vx4I9XrOVNrB.P954QlZytkrO/Kd4/lIvKJ3EtLRXmOK', 1992262168, 'FEMALE',
        '1992-01-15',
        'FUQ', now()),                                                  --ahBp5wE2
       (29896, 'Shawn', 'Bryce', 'Moses', 'mbryce3@google.pl',
        '$2a$10$PE5cRK.JC4FEvfkYE2CzGOdkbHdNkOAVZu3qMTpaxRUHTFERPyBFC', 9189653450,
        'ATTACKHELICOPTER', '1999-11-09', NULL, now()),                 --8jGHmgC9
       (50208, 'Alberta', 'Duplan', 'Cyndia', 'cduplan4@digg.com',
        '$2a$10$elcmJDUtYDVui1Drkp8X.uskNifTMTRHEuSV8tTGgcKJ/wQ5SWMRG', 5322804397,
        'ATTACKHELICOPTER', '2003-04-04', NULL, now()),                 --uboexQQ9
       (42173, 'Jade', 'Tolliday', 'Jerrilee', 'jtolliday5@time.com',
        '$2a$10$RmZD6n0KOP5yvZVAVQKo0OFfwV8hHRdKLcPRpjYukUG5GtR5LVQCu', 9129641491, 'FEMALE',
        '1990-07-21',
        'FUCK OUT', now()),                                             --AmVorHZ5
       (15435, 'Naoma', 'Skaife dIngerthorpe', 'Keri', 'kskaifedingerthorpe6@etsy.com',
        '$2a$10$OXERkUcRmadpwzRbiuoQ5.qJPYTFJkA5eNRTZTlG1pRB5voxhEIX.', 3174147421, 'MALE',
        '1998-05-29',
        'FUQ', now()),                                                  --w5KYF9K6
       (19510, 'Quintus', 'Coltan', 'Silva', 'scoltan7@tripod.com',
        '$2a$10$CKJHHYYNnBzooRNwNoYYWOUi8..W58dPRJw4VsqhuWXKexvt8OfEe', 2919857366, 'FEMALE',
        '1995-12-26',
        NULL, now()),                                                   --qkAbzWE5
       (41056, 'Hardy', 'Reggiani', NULL, 'hreggiani8@cisco.com',
        '$2a$10$bGIImmNQNsQ92V5uD/qrw.6On6siIC3Np16PQXVx9OS7B1IRCmJVa', 2983155702, 'FEMALE',
        '1992-03-31',
        NULL, now()),                                                   --9O40u3O5
       (37324, 'Auguste', 'Pladen', 'Jasmin', 'jpladen9@google.pl',
        '$2a$10$YZw5l7Se/HKTa664r9IZHeZRFyY5iPSqtDzqeVQQC2rU7LDS37gUm', 8931514969, 'MALE',
        '1996-03-21',
        NULL, now()),                                                   --Nm0z9CW8
       (20085, 'Myrta', 'Cauthra', 'Jessalyn', 'jcauthraa@npr.org',
        '$2a$10$Q.K4CoVKxRHzjZAw4sWb1OSG27ZAjjAfSCeBEKXAqVuj1ARLLEX9u', 6561849442,
        'ATTACKHELICOPTER', '2000-01-01', NULL, now()),                 --zAztgfZ9
       (67266, 'Angie', 'Whitman', NULL, 'cwhitmanb@amazonaws.com',
        '$2a$10$rKij30UQWTU1Akf0.cqmtOyZCHWdEzW.ZEAZHYaxTsbUGjhvJaVbS', 1606454317, 'FEMALE',
        '2003-07-21',
        NULL, now()),                                                   --jxfLrpA8
       (10362, 'Kerrin', 'Monnoyer', 'Patrizius', 'pmonnoyerc@discovery.com',
        '$2a$10$u3Zs4pKXHptWaYpiJhcYX.KrnrAlfSbvcqWXzPrZSBlFm8SQHXvbC', 7943320538,
        'ATTACKHELICOPTER', '2000-01-01', NULL, now()),                 --YdKVZEU8
       (5109, 'Pia', 'Abate', NULL, 'yabated@howstuffworks.com',
        '$2a$10$l34OicNY8cSltmDDrn6Vru7JsY/OP5xK9usiQaMGSBgM2/4UluW.2', 5901495893, 'MALE',
        '1995-05-20',
        NULL,
        now()),                                                         --6HSMPhJ6
       (2310, 'Floyd', 'Cumming', 'Suki', 'scumminge@i2i.jp',
        '$2a$10$MF8pDcpER20b5/gObSGyQ.FTFTMzGKlLgUXlXXPbfNV/XfNqp8IkK', 4608050773, 'FEMALE',
        '1999-06-01',
        'FUCK OUT', now()),                                             --DkNv44X5
       (51307, 'Biddy', 'Rehn', 'Andrej', 'arehnf@typepad.com',
        '$2a$10$y5NIpmjZCpj.CmNqVS.3wuqTr78a4R8x6dDn1.sVrJqdoNhxIdLcO', 2267478245,
        'ATTACKHELICOPTER', '1996-01-09', NULL, now()),                 --4TQmF2A6
       (29058, 'Chester', 'Pates', 'Marlowe', 'mpatesg@hhs.gov',
        '$2a$10$Y5Z0w7PvB9m1S6AlJbiyRe43vxshuBC6CXpRnczYio2CeIHuaWSva', 6198094300, 'FEMALE',
        '1999-06-11',
        'Myself description', now()),                                   --din8sCU6
       (10750, 'Ilene', 'Quibell', 'Lind', 'lquibellh@usa.gov',
        '$2a$10$gUyxUWF0knvxJ32bgUh8yuMQCpHcUOcUZV..ZzzEhRHDUpXhy7tHC', 9539564977,
        'ATTACKHELICOPTER', '2000-08-12', NULL, now()),                 --LESv7UO3
       (36528, 'Therine', 'Eby', 'Heath', 'hebyi@privacy.gov.au',
        '$2a$10$rBLISNyEu9EPsMxdnez9dOYQ5KUg5T59q7at1BrJX/vx70hNyesb6', 8322592462, 'FEMALE',
        '1999-11-06',
        NULL, now()),                                                   --xRQZgPS9
       (31436, 'Kev', 'Shaudfurth', 'Ronnie', 'rshaudfurthj@seattletimes.com',
        '$2a$10$2zY3Y5GGviuRCIFJ8rOiJOeE2RIQ9hPtmTXXRlq8ilW1jutlzWIRS', 7633640857,
        'ATTACKHELICOPTER', '1997-07-09', 'Myself description', now()), --YjYzhsU1
       (51093, 'Salvador', 'Hoyle', 'Miner', 'mhoylek@state.gov',
        '$2a$10$XiVZXQsgvcBiO3YgYsWE5uriKVaStqje/cYtxY/f1BdrmX.dOhAD2', 4529847107,
        'ATTACKHELICOPTER', '2003-10-07', NULL, now()),                 --TOVSoNW8
       (55807, 'Alexandro', 'Dugdale', 'Trefor', 'tdugdalel@berkeley.edu',
        '$2a$10$hwp.Vnw2tPPE7W/ypm040uhEF6S2obuNgxzjUTvsGferfdrpzOIVK', 2206029727, 'FEMALE',
        '1999-09-12',
        NULL, now()),                                                   --pp3LITF5
       (67143, 'Bidget', 'Reidshaw', 'Jo-ann', 'jreidshawm@google.pl',
        '$2a$10$k8dPs6TGIf5SaU3niajfwu1lWJIE75efuU1sqJW1olGzJmBcY4Lma', 7982842116, 'FEMALE',
        '2003-02-05',
        NULL, now()),                                                   --wHiqLcI0
       (22678, 'Lynne', 'Seatter', 'Barris', 'bseattern@weebly.com',
        '$2a$10$9f1wq7eKc4IwNI76NeJT4.ER0v0FTnyy9YIgZ1wQmwWDh9js/D/NW', 9361812990, 'FEMALE',
        '1998-02-04',
        NULL, now()),                                                   --Q3QLNfH5
       (29521, 'Velvet', 'Iacomo', NULL, 'kiacomoo@uol.com.br',
        '$2a$10$s9eCnTFG3/7dKZODbt/1ReHcXJIFasa8ubeGaPH0k3Ijz9yT0bqwy', 2146829174, 'MALE',
        '1990-06-03',
        'Myself description', now()),                                   --SwlBbJK5
       (21984, 'Kendra', 'Fells', 'Betsy', 'bfellsp@google.ca',
        '$2a$10$OJn9RAGLvSP3mnFde7oRjOViW3F1clT3TPiFOMnrGzikHAPYqLTHG', 4016788510, 'FEMALE',
        '1997-02-20',
        'Myself description', now()),                                   --BpziZNU7
       (48384, 'Killian', 'Dedam', 'Mab', 'mdedamq@latimes.com',
        '$2a$10$G7Um9y4c.E4Zg.UAGRH/oeEeui00UJmpIj/GnePFqt4tmnTVUaYwq', 3976003098, 'FEMALE',
        '1999-02-23',
        'desc', now()),                                                 --6mePSBW2
       (19306, 'Caren', 'Camerana', 'Rosaline', 'rcameranar@ucoz.ru',
        '$2a$10$Rmb5MJuQYCS5DakNeh.WAumY48vs.YCZwdoumsC7ld7qBUEgFJSUq', 8915334716, 'MALE',
        '1994-12-06',
        'FUCK OUT', now()),                                             --RKN7y9V6
       (25027, 'Alexis', 'Peperell', NULL, 'fpeperells@trellian.com',
        '$2a$10$F6HAunG4pZSbL77WFonKOe8OBf36TMm3VSdSXJjNPO/L7Ga7HC55m', 1807658920, 'FEMALE',
        '1993-04-20',
        NULL, now()),                                                   --0lgOiLK4
       (14977, 'Darn', 'Schuricht', NULL, 'dschurichtt@wsj.com',
        '$2a$10$5FolbwKVIPXkL7uSP5zsUOZkGjBGV9GjcjNVfSsERnTZ7c8SZluwK', 2916798323,
        'ATTACKHELICOPTER', '1986-04-02', NULL, now()),                 --S1AJthA1
       (14221, 'Merralee', 'Janjusevic', 'Ami', 'ajanjusevicu@senate.gov',
        '$2a$10$kKPDinkLDSMSAVmbnqWDzO2mG8l6rRtYPhxXgoiWUcgy6C5g4p.5m', 1197820039, 'FEMALE',
        '1992-02-26',
        NULL, now()),                                                   --PpgakqW5
       (10595, 'Courtney', 'Leathers', 'Dolorita', 'dleathersv@ning.com',
        '$2a$10$vSr0PV8QTn4MI3A6HSIBvu1GRQ4n0/1fJRSefDd2ekzD/9c03d4Ue', 9398844574,
        'ATTACKHELICOPTER', '1990-02-24', 'FUCK OUT', now()),           --FaCXdvA8
       (10025, 'Ashleigh', 'Bemand', 'Clywd', 'cbemandw@zdnet.com',
        '$2a$10$zss8I7lsYxoKVVN3vDYtKOkURyqPeYQRqGX0TuJ6tuD6t6fuxza.O', 8926193112, 'MALE',
        '2003-01-08',
        NULL,
        now()),                                                         --t48IrPK8
       (42379, 'Ashleigh', 'Moules', 'Issiah', 'imoulesx@princeton.edu',
        '$2a$10$5bMSjnwn.ROFSrxD4lCY2.VPWCxF2DYWSe9tf9a/nvfurUun1nxGe', 5509801157,
        'ATTACKHELICOPTER', '1999-05-09', NULL, now()),                 --ZO3xpcS9
       (62927, 'Mufinella', 'Favela', 'Dane', 'dfavelay@wikipedia.org',
        '$2a$10$uzLKShHLrOML4X808UMnMe4BE8GwZrKhxyl0x/rzntF.5VlaDbzOa', 2628624900, 'FEMALE',
        '1992-10-21',
        NULL, now()),                                                   --UbQpCxQ3
       (67825, 'Aloisia', 'Eberlein', NULL, 'heberleinz@marketwatch.com',
        '$2a$10$f8nOJigqDNRRHu2wyDZf0OL0RchaSXTPMp7g8A5uSMFqvfr2IBg02', 4353548966, 'FEMALE',
        '1997-12-29',
        NULL, now()),                                                   --9bzeb9O5
       (1994, 'Colleen', 'Thurby', NULL, 'athurby10@hhs.gov',
        '$2a$10$n5lG9L5/43iK5ZTxR5tepepuTE68HiWj9yyGfJwrTzaukcVUCSPqC', 7901185242, 'MALE',
        '1993-09-11',
        'Myself description', now()),                                   --Gjz9N1Q6
       (65332, 'Joanna', 'Malafe', 'Pru', 'pmalafe11@twitpic.com',
        '$2a$10$sO40Z6WbKrugkCss7xTWseNpkxkIHWcbw.ff4Hus8WEsrNwFqx5zO', 7075873903, 'FEMALE',
        '2003-06-05',
        NULL, now()),                                                   --mDwD3cU5
       (4168, 'Artair', 'Hinnerk', 'Julita', 'jhinnerk12@reverbnation.com',
        '$2a$10$MyBaX7on9OJLgJJ0V/EDlO92BL/MgCF0GU0JsAEqT839ryb2N2syS', 7304849738, 'MALE',
        '2002-12-07',
        'FUQ', now()),                                                  --gEoeyVU8
       (34012, 'Ange', 'Hunston', NULL, 'ehunston13@columbia.edu',
        '$2a$10$U4C3nYzX9/G3Pb8DhgvR4etziKPFwOyNaAv8Wko5YB2CVz7fBTvL6', 1869380427,
        'ATTACKHELICOPTER', '1994-12-18', NULL, now()),                 --BX14WiY9
       (7689, 'Cris', 'McSporrin', 'Dickie', 'dmcsporrin14@plala.or.jp',
        '$2a$10$KoCnsoX8AHtZf7xDEBZE0./HjC7hwnjf04SUMTDwlUJb2dvNVQOKm', 4943435997, 'FEMALE',
        '2002-05-13',
        'Myself description', now()),                                   --Sre3yGT4
       (44711, 'Esmeralda', 'Nusche', 'Benn', 'bnusche15@vkontakte.ru',
        '$2a$10$jH7cZgiUkvtk3tUvHbk11OyB3OQGqpmid0bK9O/MaFl9ZnbmLCzq.', 9864966939,
        'ATTACKHELICOPTER', '1995-03-02', NULL, now()),                 --PoGwwLC8
       (54135, 'Ingar', 'Triplet', 'Felic', 'ftriplet16@earthlink.net',
        '$2a$10$uFKrSC0/rGfRp.cvXawtYe2rAEOAT/9GJz0ks6NA7k3Q2F/u19yC2', 7336266500, 'FEMALE',
        '1996-09-02',
        'FUCK OUT', now()),                                             --44xGBVS2
       (70971, 'Otes', 'Roja', NULL, 'troja17@miibeian.gov.cn',
        '$2a$10$u/t/1Su6NXCaEpj7W2Kz1uXeAnkrQtg6YThhWocQ7X0I1HgdXcowG', 6245866892, 'FEMALE',
        '1990-08-12',
        'desc', now()),                                                 --XkdbqFP2
       (68685, 'Zared', 'Doherty', 'Vincents', 'vdoherty18@fda.gov',
        '$2a$10$VIjONkXJeue0bHrlcc/69.TNY160jwjash6CRK.Hc1iDkJ5MBUCQK', 1218608784, 'MALE',
        '2000-05-26',
        NULL,
        now()),                                                         --G4my3IG7
       (18371, 'Barby', 'Buick', NULL, 'gbuick19@jiathis.com',
        '$2a$10$DqelzqbrQ5kbKGLFB/L4L.BnEXEpF7GK6o26r064dhQ/WWW7QBWv2', 5166134797, 'MALE',
        '1999-04-07',
        'desc', now()),                                                 --nlZfICR7
       (24604, 'Ruby', 'McComish', 'Ximenes', 'xmccomish1a@nps.gov',
        '$2a$10$OtcTAkdHeANS4fZaFSeZc.ZYtPFjas5nZu/JmmMpheIndwE4R4qoy', 6993339923,
        'ATTACKHELICOPTER', '2003-10-09', 'FUQ', now()),                --TwiDa5C1
       (34144, 'Enriqueta', 'Budnik', 'Orton', 'obudnik1b@narod.ru',
        '$2a$10$n3uY.Ww30JFWQzwHfFdqGO5bHU6V6fY4IEpLFtgrJA.M75TunwpJO', 8166899427, 'MALE',
        '1996-06-21',
        NULL,
        now()),                                                         --5LBGJdC2
       (67127, 'Reade', 'Mayall', 'Imogene', 'imayall1c@kickstarter.com',
        '$2a$10$wpEuL9fFFlWhBTU0NjTsPe385alSFp5nKLcIHeSYYL0UmPMDtW.Fe', 5559845250, 'FEMALE',
        '2000-05-03',
        NULL, now()),                                                   --qLE8efX4
       (14172, 'Merill', 'Hanse', NULL, 'shanse1d@xing.com',
        '$2a$10$YjdJSOWAOMQ.U0D9Sh51ien57vJ6BvIA6aP3SLVHNrxGTX4OnBlWi', 9792993155,
        'ATTACKHELICOPTER', '2003-10-23', NULL, now())                  --5mlDEYT5
ON CONFLICT DO NOTHING;