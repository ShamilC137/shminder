CREATE TABLE countries
(
    id           SERIAL PRIMARY KEY,
    name         VARCHAR(64) NOT NULL UNIQUE,
    country_code INTEGER     NOT NULL,
	total_locations_count INTEGER NOT NULL DEFAULT 0 CHECK (total_locations_count >= 0)
);

CREATE TABLE locations
(
    id            SERIAL PRIMARY KEY,
    fk_country_id INTEGER      NOT NULL REFERENCES countries ON DELETE CASCADE ON UPDATE CASCADE,
    city          VARCHAR(256) NOT NULL UNIQUE,
	total_users_count INTEGER NOT NULL DEFAULT 0 CHECK (total_users_count >= 0)
);

CREATE TABLE users
(
    id             SERIAL PRIMARY KEY,
    fk_location_id INTEGER                                NOT NULL REFERENCES locations ON DELETE CASCADE ON UPDATE CASCADE,
    first_name     VARCHAR(32)                            NOT NULL CHECK (first_name ~ '^[a-zA-Zа-яА-Я]+$'),
    last_name      VARCHAR(32)                            NOT NULL CHECK (first_name ~ '^[a-zA-Zа-яА-Я]+$'),
    middle_name    VARCHAR(32) CHECK (first_name ~ '^[a-zA-Zа-яА-Я]+$'),
    email          VARCHAR(64)                            NOT NULL UNIQUE,
    password       VARCHAR(256)                           NOT NULL,
    phone          BIGINT                                 NOT NULL CHECK (phone BETWEEN 1000000000 AND 9999999999),
    enabled        BOOLEAN                  DEFAULT TRUE  NOT NULL,
    created_at     TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    gender         VARCHAR(16)                            NOT NULL CHECK (gender ~ '^[a-zA-Zа-яА-Я]+$'),
    age            SMALLINT                               NOT NULL CHECK (age > 17),
    about          VARCHAR(1024)
);

CREATE INDEX user_email_index ON users (email);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(16) NOT NULL UNIQUE
);

CREATE INDEX role_name_index ON roles (name);

CREATE TABLE users_roles
(
    fk_user_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
    fk_role_id INTEGER NOT NULL REFERENCES roles ON DELETE CASCADE ON UPDATE CASCADE,
    UNIQUE (fk_user_id, fk_role_id)
);
CREATE INDEX users_roles_user_index ON users_roles (fk_user_id);
CREATE INDEX users_roles_role_index ON users_roles (fk_role_id);

CREATE TABLE passions
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(16) NOT NULL UNIQUE,
	total_users_count INTEGER NOT NULL DEFAULT 0 CHECK (total_users_count >= 0)
);

CREATE INDEX passion_name_index ON passions (name);

CREATE TABLE users_passions
(
    fk_user_id    INTEGER NOT NULL REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE,
    fk_passion_id INTEGER NOT NULL REFERENCES passions ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE (fk_user_id, fk_passion_id)
);

CREATE INDEX users_passions_user_index ON users_passions (fk_user_id);
CREATE INDEX users_passions_passion_index ON users_passions (fk_passion_id);

CREATE TABLE likes
(
    fk_initiator_id INTEGER NOT NULL REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE,
    fk_victim_id    INTEGER NOT NULL REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE (fk_initiator_id, fk_victim_id)
);

CREATE INDEX likes_initiator_index ON likes (fk_initiator_id);
CREATE INDEX likes_victim_index ON likes (fk_victim_id);
ALTER TABLE likes
    ADD CONSTRAINT check_selflike CHECK (fk_initiator_id <> likes.fk_victim_id);

CREATE TABLE applications
(
    id                 SERIAL PRIMARY KEY,
    fk_user_id         INTEGER                  NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
    fk_desired_role_id INTEGER                  NOT NULL REFERENCES roles ON DELETE CASCADE ON UPDATE CASCADE,
    created_at         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    status             INTEGER                  NOT NULL DEFAULT 0
);

CREATE INDEX application_user_id_index ON applications (fk_user_id);
CREATE INDEX application_desired_role_index ON applications (fk_desired_role_id);