ALTER TABLE coursework.users
DROP COLUMN image;

CREATE TABLE coursework.images
(
    fk_user_id INTEGER PRIMARY KEY REFERENCES coursework.users,
    image BYTEA NOT NULL
);