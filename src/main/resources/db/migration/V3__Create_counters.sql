CREATE FUNCTION passions_on_insert_to_users_passions() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.passions
		SET total_users_count = total_users_count + 1
		WHERE NEW.fk_passion_id = id;
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_passions_on_insert
AFTER INSERT
ON coursework.users_passions
FOR EACH ROW
EXECUTE PROCEDURE passions_on_insert_to_users_passions();

CREATE FUNCTION locations_on_insert_to_users() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.locations
		SET total_users_count = total_users_count + 1
		WHERE NEW.fk_location_id = id;
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_locations_on_insert
AFTER INSERT
ON coursework.users
FOR EACH ROW
EXECUTE PROCEDURE locations_on_insert_to_users();

CREATE FUNCTION countries_on_insert_to_locations() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.countries
		SET total_locations_count = total_locations_count + 1
		WHERE NEW.fk_country_id = id;
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_countries_on_insert_to_locations
AFTER INSERT
ON coursework.locations
FOR EACH ROW
EXECUTE PROCEDURE countries_on_insert_to_locations();

CREATE FUNCTION passions_on_delete_users_passions() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.passions
		SET total_users_count = total_users_count - 1
		WHERE OLD.fk_passion_id = id;
		
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_passions_on_delete_users_passions
AFTER DELETE
ON coursework.users_passions
FOR EACH ROW
EXECUTE PROCEDURE passions_on_delete_users_passions();

CREATE FUNCTION locations_on_delete_users() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.locations
		SET total_users_count = total_users_count - 1
		WHERE OLD.fk_location_id = id;
		
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_locations_on_delete
AFTER DELETE
ON coursework.users
FOR EACH ROW
EXECUTE PROCEDURE locations_on_delete_users();

CREATE FUNCTION countries_on_delete_locations() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.countries
		SET total_locations_count = total_locations_count - 1
		WHERE OLD.fk_country_id = id;
	
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_countries_on_delete_locations
AFTER DELETE
ON coursework.locations
FOR EACH ROW
EXECUTE PROCEDURE countries_on_delete_locations();

CREATE FUNCTION passions_on_truncate_users_passions() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.passions
		SET total_users_count = 0;
		
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_passions_on_truncate_users_passions
AFTER TRUNCATE
ON coursework.users_passions
EXECUTE PROCEDURE passions_on_truncate_users_passions();

CREATE FUNCTION locations_on_truncate_users() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.locations
		SET total_users_count = 0;
		
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_locations_on_truncate
AFTER TRUNCATE
ON coursework.users
EXECUTE PROCEDURE locations_on_truncate_users();

CREATE FUNCTION countries_on_truncate_locations() RETURNS TRIGGER
	AS
$$
	BEGIN
		UPDATE coursework.countries
		SET total_locations_count = 0;
		
		RETURN OLD;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_countries_on_truncate_locations
AFTER TRUNCATE
ON coursework.locations
EXECUTE PROCEDURE countries_on_truncate_locations();