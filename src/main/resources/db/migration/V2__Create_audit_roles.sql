-- audit
CREATE TABLE roles_audit
(
	id SERIAL PRIMARY KEY,
	modified_at TIMESTAMP NOT NULL DEFAULT NOW(),
	operation_type VARCHAR(16) NOT NULL
);

CREATE FUNCTION roles_on_insert() RETURNS TRIGGER
	AS
$$
	BEGIN
		INSERT INTO roles_audit(operation_type)
		VALUES ('INSERT');
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_audit_on_insert_to_roles
AFTER INSERT
ON coursework.roles
FOR EACH ROW
EXECUTE PROCEDURE roles_on_insert();

CREATE FUNCTION roles_on_update() RETURNS TRIGGER
	AS
$$
	BEGIN
		INSERT INTO roles_audit(operation_type)
		VALUES ('UPDATE');
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_audit_on_update_roles
AFTER UPDATE
ON coursework.roles
FOR EACH ROW
EXECUTE PROCEDURE roles_on_update();

CREATE FUNCTION roles_on_delete() RETURNS TRIGGER
	AS
$$
	BEGIN
		INSERT INTO roles_audit(operation_type)
		VALUES ('DELETE');
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_audit_on_delete_roles
AFTER DELETE
ON coursework.roles
FOR EACH ROW
EXECUTE PROCEDURE roles_on_delete();

CREATE FUNCTION roles_on_truncate() RETURNS TRIGGER
	AS
$$
	BEGIN
		INSERT INTO roles_audit(operation_type)
		VALUES ('TRUNCATE');
		
		RETURN NEW;
	END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_audit_on_truncate_roles
AFTER TRUNCATE
ON coursework.roles
EXECUTE PROCEDURE roles_on_truncate();