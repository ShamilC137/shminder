ALTER TABLE coursework.users
DROP COLUMN age;

ALTER TABLE coursework.users
ADD COLUMN date_of_birth DATE;