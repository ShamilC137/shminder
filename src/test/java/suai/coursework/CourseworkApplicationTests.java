package suai.coursework;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {CourseworkApplication.class})
class CourseworkApplicationTests {

  @Test
  void contextLoads() {
  }

}
